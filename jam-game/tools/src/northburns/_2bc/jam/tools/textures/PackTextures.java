package northburns._2bc.jam.tools.textures;

import java.io.File;
import java.util.Objects;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public class PackTextures {
	public static void main(String... args) throws Exception {
		new PackTextures().run(parseArgs(args));
	}

	private void run(PackTexturesSettings settings) {

		for (File packDir : settings.input.listFiles()) {
			if (packDir.isDirectory()) {
				TexturePacker.process(texturePackerSettings(settings), packDir.getAbsolutePath(),
						settings.output.getAbsolutePath(), packDir.getName());
			}
		}

	}

	private Settings texturePackerSettings(PackTexturesSettings settings) {
		Settings s = new Settings();
		/*
		 * TODO: Settings
		 */
		s.maxWidth = 2048;
		s.maxHeight = 2048;
		return s;
	}

	private static class PackTexturesSettings {

		public File input;
		public File output;

		public PackTexturesSettings(File input, File output) {
			this.input = input;
			this.output = output;
		}

	}

	private static PackTexturesSettings parseArgs(String... args) throws Exception {
		Options options = new Options();

		options.addOption("i", "input", true, "Directory containing the dirs to pack.");
		options.addOption("o", "output", true, "Ouptut directory");

		try {
			CommandLine cmd = new DefaultParser().parse(options, args);

			File input = new File(Objects.requireNonNull(cmd.getOptionValue("i"),
					"Missing: " + options.getOption("i").getDescription()));
			File output = new File(Objects.requireNonNull(cmd.getOptionValue("o"),
					"Missing: " + options.getOption("o").getDescription()));
			if (!input.exists())
				throw new IllegalArgumentException("Input doesn't exist.");
			output.getParentFile().mkdirs();

			return new PackTexturesSettings(input, output);
		} catch (Exception e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(PackTextures.class.getName(), options);
			throw e;
		}
	}

}