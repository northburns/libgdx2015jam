package northburns._2bc.jam.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.Predicate;

public class InputChartImplementation<T> extends InputAdapter implements InputChart<T>, InputProcessor {
	private static final int NO_KEYCODE = Keys.UNKNOWN;
	private ArrayMap<T, Predicate<T>> onDown;
	private IntMap<T> keycodeToKey;
	private ObjectIntMap<T> keyToKeycode;

	public InputChartImplementation() {
		onDown = new ArrayMap<>();
		keycodeToKey = new IntMap<>();
		keyToKeycode = new ObjectIntMap<>();
	}

	@Override
	public void setInputKey(T key, int keycode) {
		keycodeToKey.put(keycode, key);
		keyToKeycode.put(key, keycode);
	}

	@Override
	public void onDown(T key, Predicate<T> operation) {
		onDown.put(key, operation);
	}

	@Override
	public boolean isDown(T key) {
		if (keyToKeycode.containsKey(key)) {
			int keycode = keyToKeycode.get(key, NO_KEYCODE);
			return Gdx.input.isKeyPressed(keycode);
		}
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycodeToKey.containsKey(keycode)) {
			T key = keycodeToKey.get(keycode);
			if (onDown.containsKey(key))
				return onDown.get(key).evaluate(key);
		}
		return false;
	}

}
