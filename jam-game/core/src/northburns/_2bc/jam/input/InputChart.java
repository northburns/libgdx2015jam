package northburns._2bc.jam.input;

import com.badlogic.gdx.utils.Predicate;

public interface InputChart<T> {
	void setInputKey(T key, int keycode);

	/**
	 * 
	 * @param key
	 * @param operation
	 *            returns true if the keydown is processed and should not be
	 *            propagated further. The key is passed as convenience (TODO:
	 *            some kind of input happening context could be useful, maybe?)
	 */
	void onDown(T key, Predicate<T> operation);

	boolean isDown(T key);

}
