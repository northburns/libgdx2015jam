package northburns._2bc.jam.constants;

/**
 * The key values match the json file names.
 *
 */
public enum FruitAnimations {
	STAGE_0("stage-0"), STAGE_4("stage-4"), PUFF("puff"),;

	public final String key;

	private FruitAnimations(String key) {
		this.key = key;
	}
}
