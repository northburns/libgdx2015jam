package northburns._2bc.jam.constants;

/**
 * The key values match the json file names.
 *
 */
public enum PlanetAnimations {
	PLANET("planet"), GRAVITY("gravity"), FACE_FROWN("face-frown"), FACE_NEUTRAL("face-neutral"), FACE_SMILE(
			"face-smile"), AIR("air"),;
	public final String key;

	private PlanetAnimations(String key) {
		this.key = key;
	}
}
