package northburns._2bc.jam.constants;

public final class HardCoded {
	public static final float VIEWPORT_WORLD_HEIGHT = 800f;

	public static final float VIEWPORT_WORLD_WIDTH = 800f;

	public static final float BACKGROUND_SCROLL_COEFFICIENT = 25f;

	/**
	 * Actually squared, but that's fine.
	 */
	public static final float PHYSICS_WALKINGSPEED_GROUNDED = 10f;

	public static final float PHYSICS_WALKINGSPEED_INAIR = 1.1f;

	public static final float PHYSICS_JUMP_IMPULSE = 8500f;

	public static final float PHYSICS_FRICTION_WALK = .3f;

	public static final float PHYSICS_FRICTION_STOP = Float.MAX_VALUE;

	public static final float PARAMS_PLANET_RADIUS = 10f;

	public static final float PARAMS_FLY_RADIUS = 20f;

	public static final float PARAMS_GRAVITYFIELD_RADIUS = 35f;

	public static final float PARAMS_GRAVITYFIELD_GRAVITYFORCE = 100f;

	/**
	 * How many seconds you have to wait before you can throw another fruit.
	 */
	public static final float PARAMS_THROW_COOLDOWN = 1f;

	/**
	 * The squared grounded walk speed treshold. If above, walking. If not,
	 * standing.
	 */
	public static final float ANIMATION_WALKSPEED_TRESHOLD = 1f;

	public static final float PARAMS_FRUIT_RADIUS = 1.5f;

	public static final float PARAMS_FRUIT_THROWANGLE = 75f;

	public static final float PARAMS_FRUIT_THROWSPEEDSQUARED = 250f;

	public static final int GRAPHICS_LAYERS_FRUIT = 11;

	public static final float PARAMS_FRUIT_DENSITY = 1f;

	public static final float PARAMS_FRUIT_RESTITUTIONCOEFFICIENT = .5f;

	public static final float PARAMS_FLY_ANGULARVEL = 32f;

	public static final float PARAMS_FLY_ALTITUTE_FREQ = 3.4f;

	public static final float PARAMS_FLY_ALTITUTE_AMPL = 4f;

	public static final float PARAMS_FLY_WIDTHHALF = 1f;

	public static final float PARAMS_FLY_HEIGHTHALF = .5f;

	public static final float STEP_HALFWIDTH = 2f;
	public static final float STEP_HALFHEIGHT = .5f;
	public static final float ROPE_MAX_LENGTH = 1.5f;

	public static final float PARAMS_FLY_LADDERCOUNTDOWN = 1f;

	public static final float WINLOSE_DRIFTING_GAMEOVERDELAY = 2f;

	public static final float WINLOSE_PLANETDIES_GAMEOVERDELAY = .5f;

	public static final int PARAMS_PLANET_HEALTH = 7;

	public static final int PARAMS_PLANET_HEALTH_FROWN_TRESHOLD = 2;

	private HardCoded() {
	}

}
