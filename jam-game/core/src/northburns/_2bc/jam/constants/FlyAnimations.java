package northburns._2bc.jam.constants;

/**
 * The key values match the json file names.
 *
 */
public enum FlyAnimations {
	FLY_LEFT_SIZE10("fly_left_size_10"),;

	public final String key;

	private FlyAnimations(String key) {
		this.key = key;
	}
}
