package northburns._2bc.jam.constants;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

public enum TextureConstants {
	SPACEBG_1("static/space.png"), TITLE("ui/title.png"), SCREEN_INTRO("screens/INTRO.png"), SCREEN_LOSE_DRIFT(
			"screens/lose-drift.png"), SCREEN_LOSE_HEALTH("screens/lose-health.png"), SCREEN_WIN(
					"screens/win.png"), SCREEN_RAPORT("screens/win-raport.png"), SCREEN_STAMP_A(
							"screens/win-stamp-A.png"), SCREEN_STAMP_B("screens/win-stamp-B.png"), SCREEN_STAMP_C(
									"screens/win-stamp-C.png"),;

	public final AssetDescriptor<Texture> assetDescriptor;

	private TextureConstants(String descr) {
		this.assetDescriptor = new AssetDescriptor<>(descr, Texture.class);
	}

	public static void loadAll(AssetManager assetManager) {
		for (TextureConstants tc : values()) {
			assetManager.load(tc.assetDescriptor);
		}
	}

}
