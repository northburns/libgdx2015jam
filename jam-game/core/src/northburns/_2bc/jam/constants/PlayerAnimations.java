package northburns._2bc.jam.constants;

/**
 * The key values match the json file names.
 *
 */
public enum PlayerAnimations {
	STAND("stand"), JUMP_LEFT("jump_left"), JUMP_RIGHT("jump_right"), PARACHUTE("parachute"), THROW_LEFT(
			"throw_left"), THROW_RIGHT("throw_right"), WALK_LEFT("walk_left"), WALK_RIGHT("walk_right"),;
	public final String key;

	private PlayerAnimations(String key) {
		this.key = key;
	}
}
