package northburns._2bc.jam.constants;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public enum TextureAtlasConstants {
	PROTAGONIST(new AssetDescriptor<>("gen/pack/protagonist.atlas", TextureAtlas.class)), PLANET(
			new AssetDescriptor<>("gen/pack/planet.atlas", TextureAtlas.class)), FRUIT_A(
					new AssetDescriptor<>("gen/pack/fruit-a.atlas", TextureAtlas.class)), FLY(
							new AssetDescriptor<>("gen/pack/fly.atlas", TextureAtlas.class)),;

	public final AssetDescriptor<TextureAtlas> assetDescriptor;

	private TextureAtlasConstants(AssetDescriptor<TextureAtlas> descr) {
		this.assetDescriptor = descr;
	}

	public static void loadAll(AssetManager assetManager) {
		for (TextureAtlasConstants tc : values()) {
			assetManager.load(tc.assetDescriptor);
		}
	}

}
