package northburns._2bc.jam.constants;

public enum Sfx {
	FLY_EAT_BAD("fly_eat_bad"), FLY_EAT_GOOD("fly_eat_good"), FLY_FANFARE("fly_fanfare"), FRUIT_PUFF(
			"fruit_puff"), PROTAGONIST_JUMP("protagonist_jump"), PROTAGONIST_THROW(
					"protagonist_throw"), SCREEN_GAMEOVER_DRIFT("screen_gameover_drift"), SCREEN_GAMEOVER_HEALTH(
							"screen_gameover_health"), SCREEN_INTRO("screen_intro"), SCREEN_WIN("screen_win"), UI_CLICK(
									"ui_click"), UI_STAMP("ui_stamp"), UI_TYPEWRITER("ui_typewriter"),;
	public String key;

	Sfx(String key) {
		this.key = key;
	}
}
