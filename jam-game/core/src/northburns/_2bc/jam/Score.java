package northburns._2bc.jam;

public class Score {

	/**
	 * 5 seconds
	 */
	private static final long TIME_MIN = 5L * 1000L;
	/**
	 * 5 minutes
	 */
	private static final long TIME_MAX = 5L * 60L * 1000L;

	public long time;
	public float healthPercentage;
	/**
	 * 0 = slow; 1 = fast
	 */
	public float timePercentage;
	public float score;
	public Rating rating;

	public Score(long time, float health) {

		this.time = time;
		this.healthPercentage = health;

		calcTimePercentage();
		calcScore();
		calcRating();
	}

	private void calcScore() {
		score = healthPercentage * timePercentage;
	}

	public enum Rating {
		A, B, C
	}

	private void calcRating() {
		/*
		 * XXX: Hard coded stuff, again!
		 */
		if (score >= .9f)
			rating = Rating.A;
		else if (score >= .55f)
			rating = Rating.B;
		else
			rating = Rating.C;
	}

	private void calcTimePercentage() {
		if (time <= TIME_MIN)
			timePercentage = 0f;
		else if (time >= TIME_MAX)
			timePercentage = 1f;
		else {
			long val = time - TIME_MIN;
			long max = TIME_MAX - TIME_MIN;
			timePercentage = (float) val / (float) max;
		}
		timePercentage = 1f - timePercentage;
	}

	private static int pct(float pct) {
		return (int) (pct * 100f);
	}

	public String timeScoreString() {
		return pct(timePercentage) + "%";
	}

	public String healthScoreString() {
		return pct(healthPercentage) + "%";
	}

	public String totalScoreString() {
		return pct(score) + "%";
	}

	@Override
	public String toString() {
		return timeScoreString() + " + " + healthScoreString() + " = " + totalScoreString();
	}

	public static void main(String... args) {
		System.out.println(new Score(TIME_MIN, 1f).toString());
		System.out.println(new Score(TIME_MAX, 1f).toString());
	}
}
