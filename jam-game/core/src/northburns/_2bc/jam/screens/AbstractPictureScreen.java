package northburns._2bc.jam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.resourcehelp.JamResources;

public abstract class AbstractPictureScreen extends ScreenAdapter {
	protected JamResources res;
	protected SpriteBatch batch;
	protected Stage stage;
	private Image img;
	protected Cell<? extends Actor> customContentCell;
	protected Table table;
	private TextButton buttonContinue;

	public AbstractPictureScreen(JamResources res, SpriteBatch batch) {
		this.res = res;
		this.batch = batch;

		stage = new Stage(new ScreenViewport(), batch);
		stage.setDebugAll(false);

		img = new Image();
		img.setScaling(Scaling.fit);

		buttonContinue = new TextButton("Continue", res.skin);

		buttonContinue.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (buttonContinue.isDisabled())
					return;
				AbstractPictureScreen.this.res.sfx.playSfx(Sfx.UI_CLICK, false);
				continueButtonClicked();
			}
		});

		table = new Table(res.skin);
		table.defaults().pad(5f);
		table.add(img).expand().fill();
		customContentCell = table.add((Actor) null);
		table.row();
		table.add(buttonContinue).colspan(2).row();

		table.setFillParent(true);
		stage.addActor(table);
	}

	protected void setContinueEnabled(boolean b) {
		buttonContinue.setDisabled(!b);
	}

	protected Skin getSkin() {
		return table.getSkin();
	}

	protected abstract void continueButtonClicked();

	protected abstract void showPostOps();

	protected void changeImage(Texture t) {
		img.setDrawable(new TextureRegionDrawable(new TextureRegion(t)));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		res.backgroundRenderer.draw(batch, delta);
		stage.getViewport().apply();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		res.backgroundRenderer.stopAndSetRandom();
		Gdx.input.setInputProcessor(stage);
		showPostOps();
	}

	@Override
	public void hide() {
	}
}
