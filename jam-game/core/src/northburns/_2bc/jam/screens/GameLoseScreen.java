package northburns._2bc.jam.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import northburns._2bc.jam.Screens.ScreenName;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.demo.GameEnder.LoseState;
import northburns._2bc.jam.resourcehelp.JamResources;

public class GameLoseScreen extends AbstractPictureScreen {

	private LoseState state;

	public GameLoseScreen(JamResources res, SpriteBatch batch) {
		super(res, batch);
	}

	@Override
	protected void continueButtonClicked() {
		res.screens.changeScreen(ScreenName.TITLE);
	}

	@Override
	protected void showPostOps() {
		res.stopSong();
		Sfx sound;
		switch (state) {
		case DRIFT_TO_SPACE:
			sound = Sfx.SCREEN_GAMEOVER_DRIFT;
			break;
		case PLANET_HP_ZERO:
			sound = Sfx.SCREEN_GAMEOVER_HEALTH;
			break;
		default:
			throw new IllegalStateException("Unkown state: " + state);
		}
		res.sfx.playSfx(sound, true);
	}

	public void setState(LoseState state) {
		this.state = state;
		TextureConstants image;
		switch (state) {
		case DRIFT_TO_SPACE:
			image = TextureConstants.SCREEN_LOSE_DRIFT;
			break;
		case PLANET_HP_ZERO:
			image = TextureConstants.SCREEN_LOSE_HEALTH;
			break;
		default:
			throw new IllegalStateException("Unkown state: " + state);
		}
		changeImage(res.assets.get(image.assetDescriptor));
	}

}
