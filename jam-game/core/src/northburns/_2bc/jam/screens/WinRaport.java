package northburns._2bc.jam.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.Scaling;

import northburns._2bc.jam.Score;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.resourcehelp.JamFont;
import northburns._2bc.jam.resourcehelp.JamResources;

public class WinRaport extends Widget {

	private TextureRegion raport;
	private TextureRegion stampA;
	private TextureRegion stampB;
	private TextureRegion stampC;

	private float drawWidth, drawHeight;
	private float drawXd;
	private float drawYd;
	private JamFont font;
	private float drawPct;
	private float drawHeightStamp;
	private float drawWidthStamp;

	public WinRaport(JamResources res) {
		font = res.font;

		raport = new TextureRegion(res.assets.get(TextureConstants.SCREEN_RAPORT.assetDescriptor));
		stampA = new TextureRegion(res.assets.get(TextureConstants.SCREEN_STAMP_A.assetDescriptor));
		stampB = new TextureRegion(res.assets.get(TextureConstants.SCREEN_STAMP_B.assetDescriptor));
		stampC = new TextureRegion(res.assets.get(TextureConstants.SCREEN_STAMP_C.assetDescriptor));

		setSize(getPrefWidth(), getPrefHeight());
	}

	public float getPrefWidth() {
		return raport.getRegionWidth();
	}

	public float getPrefHeight() {
		return raport.getRegionHeight();
	}

	public float getMinWidth() {
		return 0;
	}

	public float getMinHeight() {
		return 0;
	}

	private static final float FONT_SCL = 4f;
	private boolean score1;
	private boolean score2;
	private boolean score3;
	private boolean stamp;
	private Score score;

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		/*
		 * XXX: Brace for hardcoded values.
		 */
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		float drawX = getX() + drawXd;
		float drawY = getY() + drawYd;

		batch.draw(raport, drawX, drawY, getOriginX(), getOriginY(), drawWidth, drawHeight, getScaleX(), getScaleY(),
				getRotation());

		float fontScaleX = drawPct * FONT_SCL * getScaleX();
		float fontScaleY = drawPct * FONT_SCL * getScaleY();

		font.begin(batch);
		font.setColor(Color.BROWN);
		if (score1)
			font.draw(batch, score.timeScoreString(), drawX + .42f * drawWidth, drawY + .845f * drawHeight, fontScaleX,
					fontScaleY);
		if (score2)
			font.draw(batch, score.healthScoreString(), drawX + .42f * drawWidth, drawY + .646f * drawHeight,
					fontScaleX, fontScaleY);
		if (score3)
			font.draw(batch, score.totalScoreString(), drawX + .38f * drawWidth, drawY + .371f * drawHeight,
					1.7f * fontScaleX, 1.7f * fontScaleY);
		font.end(batch);

		if (stamp) {
			TextureRegion stampTexture;
			switch (score.rating) {
			case A:
				stampTexture = stampA;
				break;
			case B:
				stampTexture = stampB;
				break;
			case C:
				stampTexture = stampC;
				break;
			default:
				throw new IllegalStateException("Unkown rating in the set score.");
			}
			batch.draw(stampTexture, drawX + drawWidth - drawWidthStamp - .02f * drawWidth, drawY + .320f * drawHeight,
					getOriginX(), getOriginY(), drawWidthStamp, drawHeightStamp, getScaleX(), getScaleY(),
					getRotation());
		}

		batch.setColor(color.r, color.g, color.b, 1f);

	}

	public void setScore(Score score) {
		this.score = score;
	}

	public void showElements(boolean score1, boolean score2, boolean score3, boolean stamp) {
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
		this.stamp = stamp;
	}

	public void layout() {

		Vector2 v = Scaling.fit.apply(raport.getRegionWidth(), raport.getRegionHeight(), getWidth(), getHeight());

		drawWidth = v.x;
		drawHeight = v.y;

		drawXd = (getWidth() - drawWidth) * .5f;
		drawYd = (getHeight() - drawHeight) * .5f;

		// Could be calc'd from width as well.
		drawPct = drawHeight / (float) raport.getRegionHeight();

		final float stampMaxPct = .4f;
		v = Scaling.fit.apply(stampA.getRegionWidth(), stampA.getRegionHeight(), stampMaxPct * drawWidth,
				stampMaxPct * drawHeight);

		drawWidthStamp = v.x;
		drawHeightStamp = v.y;

	}

}
