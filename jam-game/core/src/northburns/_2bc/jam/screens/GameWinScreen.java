package northburns._2bc.jam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;

import northburns._2bc.jam.Score;
import northburns._2bc.jam.Screens.ScreenName;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.resourcehelp.JamResources;
import northburns._2bc.jam.resourcehelp.JamResources.Song;

public class GameWinScreen extends AbstractPictureScreen {

	private WinRaport raport;

	public GameWinScreen(JamResources res, SpriteBatch batch) {
		super(res, batch);

		changeImage(res.assets.get(TextureConstants.SCREEN_WIN.assetDescriptor));

		// raport = new Image(new
		// TextureRegion(res.assets.get(TextureConstants.SCREEN_RAPORT.assetDescriptor)));
		// stampA = new
		// Image(res.assets.get(TextureConstants.SCREEN_STAMP_A.assetDescriptor));
		// stampB = new
		// Image(res.assets.get(TextureConstants.SCREEN_STAMP_B.assetDescriptor));
		// stampC = new
		// Image(res.assets.get(TextureConstants.SCREEN_STAMP_C.assetDescriptor));
		// raport.setScaling(Scaling.fit);
		// stampA.setScaling(Scaling.fit);
		// stampB.setScaling(Scaling.fit);
		// stampC.setScaling(Scaling.fit);

		raport = new WinRaport(res);
	}

	@Override
	protected void showPostOps() {
		res.playSong(Song.THEME);
		res.sfx.playSfx(Sfx.SCREEN_WIN, true);

		customContentCell.setActor(raport).expand().fill();
		// TODO: Doesn't center yet?... or does it?
		customContentCell.center();

		final float DURATION_FADEIN = 2f;
		final float DURATION_PRE_SCORE = .5f;
		final float DURATION_POST_SCORE = 2.1f;
		final float DURATION_SHAKE = 1f;
		final float SCALE_PRESHAKE = .9f;

		raport.showElements(false, false, false, false);
		raport.getColor().a = 0f;
		raport.setScale(0f, 0f);
		raport.addAction(Actions.parallel(Actions.fadeIn(DURATION_FADEIN), Actions.scaleTo(1f, 1f, DURATION_FADEIN)));

		setContinueEnabled(false);
		/*
		 * XXX: Uuuhmm, ok. It's a jam game! :D
		 */
		raport.addAction(Actions.delay(DURATION_FADEIN, Actions.run(new Runnable() {

			@Override
			public void run() {
				raport.addAction(Actions.delay(DURATION_PRE_SCORE, Actions.run(new Runnable() {

					@Override
					public void run() {
						res.sfx.playSfx(Sfx.UI_TYPEWRITER, true);
						raport.showElements(true, false, false, false);
						raport.addAction(Actions.delay(DURATION_POST_SCORE, Actions.run(new Runnable() {

							@Override
							public void run() {
								res.sfx.playSfx(Sfx.UI_TYPEWRITER, true);
								raport.showElements(true, true, false, false);
								raport.addAction(Actions.delay(DURATION_POST_SCORE, Actions.run(new Runnable() {

									@Override
									public void run() {
										res.sfx.playSfx(Sfx.UI_TYPEWRITER, true);
										raport.showElements(true, true, true, false);
										raport.addAction(Actions.delay(DURATION_POST_SCORE, Actions.run(new Runnable() {

											@Override
											public void run() {
												res.sfx.playSfx(Sfx.UI_STAMP, true);
												raport.showElements(true, true, true, true);
												raport.setScale(SCALE_PRESHAKE);
												raport.addAction(Actions.scaleTo(1f, 1f, DURATION_SHAKE,
														Interpolation.swingOut));
												setContinueEnabled(true);
											}
										})));
									}
								})));
							}
						})));
					}
				})));
			}
		})));

	}

	public void setScore(Score score) {
		raport.setScore(score);
	}

	public void showDebugNotice(boolean wonByDebugKey) {
		if (wonByDebugKey) {
			String message = "Debug key 'Instant Win' used";
			Gdx.app.log("DEBUG", message);
			Dialog d = new Dialog("Debug", getSkin());
			d.getContentTable().add(message);
			d.button("Dismiss");
			d.setMovable(false);
			d.setFillParent(true);
			d.setModal(true);
			d.show(stage);
		}
	}

	@Override
	protected void continueButtonClicked() {
		customContentCell.setActor(null);
		raport.getColor().a = 0f;
		raport.clearActions();
		res.screens.changeScreen(ScreenName.TITLE);
	}

}
