package northburns._2bc.jam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import northburns._2bc.jam.Screens.ScreenName;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.resourcehelp.JamResources;
import northburns._2bc.jam.resourcehelp.JamResources.Song;

public class TitleScreen extends ScreenAdapter {

	private JamResources res;
	private AssetManager assets;
	private Stage stage;
	private SpriteBatch batch;
	private Dialog help;

	public TitleScreen(final JamResources res, SpriteBatch batch) {
		this.res = res;
		this.batch = batch;
		assets = res.assets;

		assets.load(TextureConstants.TITLE.assetDescriptor);

		assets.finishLoading();

		stage = new Stage(new ScreenViewport(), batch);
		stage.setDebugAll(false);
		Gdx.input.setInputProcessor(stage);

		/*
		 * Create title screen
		 */

		createHelpWindw(res.skin);

		Image img = new Image(assets.get(TextureConstants.TITLE.assetDescriptor));
		img.setScaling(Scaling.fit);

		final TextButton buttonPlay = new TextButton("Play", res.skin);
		final TextButton buttonHelp = new TextButton("Help & Credits", res.skin);
		final TextButton buttonQuit = new TextButton("Quit", res.skin);

		buttonPlay.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				res.sfx.playSfx(Sfx.UI_CLICK, false);
				res.screens.changeScreen(ScreenName.INTRO);
			}
		});
		buttonHelp.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				res.sfx.playSfx(Sfx.UI_CLICK, false);
				help.show(stage);
			}
		});
		buttonQuit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit(); // TODO: Is that the wawy to exit? See the
								// JavaDoc.
			}
		});

		final Table table = new Table(res.skin);
		table.defaults().pad(5f);
		table.add(img).expand().fill();
		table.row();
		table.add(buttonPlay).row();
		table.add(buttonHelp).row();
		table.add(buttonQuit).row();

		table.setFillParent(true);
		stage.addActor(table);

		/*
		 * Only the first time, fade in the stuff
		 */
		table.getColor().a = 0f;
		buttonPlay.getColor().a = 0f;
		buttonHelp.getColor().a = 0f;
		buttonQuit.getColor().a = 0f;
		table.addAction(Actions.delay(.3f, Actions.run(new Runnable() {

			@Override
			public void run() {
				table.addAction(Actions.fadeIn(2f));
				table.addAction(Actions.delay(2f, Actions.run(new Runnable() {

					@Override
					public void run() {
						buttonPlay.addAction(Actions.fadeIn(1f));
						buttonHelp.addAction(Actions.fadeIn(1f));
						buttonQuit.addAction(Actions.fadeIn(1f));
					}
				})));
			}
		})));
	}

	@Override
	public void show() {
		res.playSong(Song.THEME);
		res.backgroundRenderer.stopAndSetRandom();
		Gdx.input.setInputProcessor(stage);
	}

	private void createHelpWindw(Skin skin) {
		help = new Dialog("Help & Credits", res.skin) {

			@Override
			protected void result(Object object) {
				res.sfx.playSfx(Sfx.UI_CLICK, false);
				res.backgroundRenderer.stopAndSetRandom();
			}
		};
		help.setModal(true);
		help.setResizable(false);
		help.setFillParent(true);

		Label text = new Label(Gdx.files.internal("text/help.txt").readString(), skin);
		text.setWrap(true);
		ScrollPane scroll = new ScrollPane(text, skin);
		scroll.setFillParent(true);
		scroll.setFadeScrollBars(false);

		help.getContentTable().add(scroll).fill().expand();

		help.button("Close");
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		res.backgroundRenderer.draw(batch, delta);
		stage.getViewport().apply();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose() {
		super.dispose();
		stage.dispose();
	}

}
