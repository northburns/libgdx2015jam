package northburns._2bc.jam.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import northburns._2bc.jam.Screens.ScreenName;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.resourcehelp.JamResources;

public class IntroScreen extends AbstractPictureScreen {

	public IntroScreen(JamResources res, SpriteBatch batch) {
		super(res, batch);
		changeImage(res.assets.get(TextureConstants.SCREEN_INTRO.assetDescriptor));
	}

	@Override
	protected void continueButtonClicked() {
		res.screens.changeScreen(ScreenName.GAME);
	}

	@Override
	protected void showPostOps() {
		res.stopSong();
		res.sfx.playSfx(Sfx.SCREEN_INTRO, true);
	}

}
