package northburns._2bc.jam;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.utils.ArrayMap;

import northburns._2bc.jam.constants.TextureAtlasConstants;
import northburns._2bc.jam.constants.TextureConstants;
import northburns._2bc.jam.demo.DemoScreen_Box_2;
import northburns._2bc.jam.demo.GameEnder;
import northburns._2bc.jam.resourcehelp.JamFont;
import northburns._2bc.jam.resourcehelp.JamResources;
import northburns._2bc.jam.resourcehelp.ShaderSupplier;
import northburns._2bc.jam.resourcehelp.impl.ShaderSupplierImpl;
import northburns._2bc.jam.screens.GameLoseScreen;
import northburns._2bc.jam.screens.GameWinScreen;
import northburns._2bc.jam.screens.IntroScreen;
import northburns._2bc.jam.screens.TitleScreen;

public class JamGame extends Game implements GameEnder, Screens {
	SpriteBatch batch;
	Texture img;
	AssetManager assets;
	private ArrayMap<ScreenName, Screen> screens;
	private JamFont font;
	private ShaderSupplier shaders;
	private JamResources res;

	@Override
	public void create() {
		Box2D.init();

		batch = new SpriteBatch();
		batch.enableBlending();
		assets = new AssetManager();
		TextureConstants.loadAll(assets);

		shaders = new ShaderSupplierImpl();
		font = new JamFont(shaders);

		res = new JamResources(assets, font, shaders, this);

		assets.load(TextureAtlasConstants.PROTAGONIST.assetDescriptor);
		assets.load(TextureAtlasConstants.PLANET.assetDescriptor);
		assets.load(TextureAtlasConstants.FRUIT_A.assetDescriptor);
		assets.load(TextureAtlasConstants.FLY.assetDescriptor);

		assets.finishLoading();

		screens = new ArrayMap<>();

		screens.put(ScreenName.GAME, new DemoScreen_Box_2(this, res, batch));
		screens.put(ScreenName.TITLE, new TitleScreen(res, batch));
		screens.put(ScreenName.WIN, new GameWinScreen(res, batch));
		screens.put(ScreenName.LOSE, new GameLoseScreen(res, batch));
		screens.put(ScreenName.INTRO, new IntroScreen(res, batch));

		setScreen(screens.get(ScreenName.TITLE));
		// win(5L * 1000L + 1L, .5f, false);
	}

	@Override
	public void changeScreen(ScreenName screen) {
		res.sfx.stopAll();
		JamGame.this.setScreen(screens.get(screen));
	}

	@Override
	public void render() {
		super.render();
		/*
		 * XXX: Ok, so. I set a color to some widget. The color doesn't get
		 * restarted anywhere. It looks like I have to reset it myself..
		 */
		batch.setColor(1f, 1f, 1f, 1f);
	}

	@Override
	public void dispose() {
		super.dispose();
		assets.dispose();
		batch.dispose();
		font.dispose();
		shaders.dispose();
		for (Screen screen : screens.values())
			screen.dispose();
	}

	@Override
	public void resize(int width, int height) {
		res.resize(width, height);
		super.resize(width, height);
	}

	@Override
	public void win(long time, float health, boolean wonByDebugKey) {
		GameWinScreen winScreen = (GameWinScreen) screens.get(ScreenName.WIN);
		/*
		 * TODO: Cast to the winscreen, set params, and change to it
		 */
		winScreen.setScore(new Score(time, health));
		winScreen.showDebugNotice(wonByDebugKey);
		changeScreen(ScreenName.WIN);
	}

	@Override
	public void lose(LoseState state) {
		GameLoseScreen loseScreen = (GameLoseScreen) screens.get(ScreenName.LOSE);
		/*
		 * TODO: Cast to the winscreen, set params, and change to it
		 */
		loseScreen.setState(state);
		changeScreen(ScreenName.LOSE);
	}
}
