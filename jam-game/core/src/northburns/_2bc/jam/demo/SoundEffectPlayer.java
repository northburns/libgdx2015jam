package northburns._2bc.jam.demo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.LongArray;
import com.badlogic.gdx.utils.ObjectMap;

import northburns._2bc.jam.constants.Sfx;

/**
 * The most na�ve sound effect player. Let's hope it doesn't play a single
 * effect a million times at the same time..
 */
public class SoundEffectPlayer implements Disposable {

	private final ObjectMap<Sfx, Sound> sounds;
	private ObjectMap<Sfx, LongArray> stoppables;

	public SoundEffectPlayer() {
		sounds = new ObjectMap<>(Sfx.values().length, 1f);
		stoppables = new ObjectMap<>(Sfx.values().length, 1f);
		for (Sfx sfx : Sfx.values()) {
			Sound s = Gdx.audio.newSound(Gdx.files.internal("sfx/" + sfx.key + ".mp3"));
			sounds.put(sfx, s);
			stoppables.put(sfx, new LongArray());
		}
	}

	public void stopAll() {
		for (Sfx sfx : stoppables.keys()) {
			Sound s = sounds.get(sfx);
			while (stoppables.get(sfx).size > 0)
				s.stop(stoppables.get(sfx).pop());
		}
	}

	public void playSfx(Sfx sfx, boolean stoppable) {
		Sound s = sounds.get(sfx);
		long id = s.play();
		if (stoppable)
			stoppables.get(sfx).add(id);
	}

	@Override
	public void dispose() {
		for (Sound s : sounds.values())
			s.dispose();
	}

}
