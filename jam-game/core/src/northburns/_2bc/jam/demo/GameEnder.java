package northburns._2bc.jam.demo;

public interface GameEnder {

	public enum LoseState {
		PLANET_HP_ZERO, DRIFT_TO_SPACE
	}

	/**
	 * 
	 * @param time
	 *            in milliseconds
	 * @param health
	 *            percentage
	 * @param wonByDebugKey
	 */
	void win(long time, float health, boolean wonByDebugKey);

	void lose(LoseState state);

}
