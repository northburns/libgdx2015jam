package northburns._2bc.jam.demo;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

abstract class AbstractDemoScreen extends ScreenAdapter {
	protected final AssetManager assets;
	protected final SpriteBatch batch;
	protected final OrthographicCamera camera;
	protected final Viewport viewport;

	public AbstractDemoScreen(SpriteBatch batch, AssetManager assets) {
		this(null, batch, assets);
	}

	public AbstractDemoScreen(Viewport viewport, SpriteBatch batch, AssetManager assets) {
		this.batch = batch;
		this.assets = assets;

		camera = new OrthographicCamera();
		if (viewport == null)
			this.viewport = new ScreenViewport(camera);
		else {
			this.viewport = viewport;
			viewport.setCamera(camera);
		}
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
}
