package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class PlanetComponent implements Component {

	public int health;
	public Entity face;
	public Entity gravity;
	public Entity air;

	public PlanetComponent(int health, Entity face, Entity gravity, Entity air) {
		this.health = health;
		this.face = face;
		this.gravity = gravity;
		this.air = air;
	}

}
