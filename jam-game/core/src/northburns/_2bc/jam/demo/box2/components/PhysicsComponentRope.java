package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.utils.Array;

public class PhysicsComponentRope implements Component {

	public Entity flyToFollow;
	public BodyForRope physics;

	public static class BodyForRope {

		public Array<Joint> joints;
		public Array<Body> bodies;
		public Body topStep;

	}
}
