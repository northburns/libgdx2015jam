package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.demo.box2.components.FlyData;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;
import northburns._2bc.jam.resourcehelp.JamResources;

public class FlySystem extends IteratingSystem {

	private Contacts contacts;

	private JamResources jam;

	/**
	 * XXX: I dislike couplign these two systems like this.
	 */
	private PhysicsSystem physicsSystem;

	public FlySystem(PhysicsSystem physicsSystem, Contacts contact, JamResources jam, int priority) {
		super(Family.all(FlyData.class, PhysicsComponent.class).get(), priority);
		this.physicsSystem = physicsSystem;
		contacts = contact;
		this.jam = jam;
	}

	/**
	 * A temp vector used for calcs. No need to pollute the heap.
	 */
	private Vector2 v = new Vector2();

	public static PhysicsComponent createFlyPhysics(float scale) {
		PhysicsComponent body = new PhysicsComponent();
		body.bodyType = PhysicsComponent.BodyType.FLY;
		/*
		 * XXX: Move fly's physics&anim params somewhere?
		 */
		body.bodyTypeParams = new float[] { scale * HardCoded.PARAMS_FLY_WIDTHHALF,
				scale * HardCoded.PARAMS_FLY_HEIGHTHALF };
		body.creationPoint = new Vector2(HardCoded.PARAMS_FLY_RADIUS, 0f);
		body.posToAnimOrigin.set(0f, 0f);
		return body;
	}

	/**
	 * XXX: Monolithic method...
	 */
	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		FlyData fly = ComponentMappers.FLY_DATA.get(entity);
		PhysicsComponent physics = ComponentMappers.PHYSICS_COMPONENT.get(entity);

		/*
		 * Fruit stuff
		 */
		if (contacts.flyTouchesFruit.containsKey(entity)) {
			// Full sized don't eat no more.www
			if (fly.size.canGrow()) {
				Array<Entity> fruits = contacts.flyTouchesFruit.get(entity);
				if (fruits.size > 0) {
					/*
					 * We handle just one fruit at a time (in case we add a
					 * "eating cooldown" for the fly)
					 */
					// TODO: Add an "eating cooldown"

					Entity fruit = fruits.first();
					boolean wasGood = ComponentMappers.FRUIT_DATA.get(fruit).edible;

					if ((wasGood && fly.size.canGrow()) || (!wasGood && fly.size.canShrink())) {

						getEngine().removeEntity(fruit);
						if (wasGood)
							jam.sfx.playSfx(Sfx.FLY_EAT_GOOD, true);
						else
							jam.sfx.playSfx(Sfx.FLY_EAT_BAD, true);

						/*
						 * Change fly's size
						 */
						fly.size = fly.size.getNext(wasGood);

						// entity.add(FlySystem.createFlyPhysics(fly.size.scale));
						// That doesn't work.. the PhysicsSystem (which handles
						// the
						// Box2d stuff) doesn't pick that up.
						/*
						 * TODO: Actually change the size of the box2d body...
						 * somehow.
						 */

						physics.creationPoint = physics.body.getPosition();
						physicsSystem.entityRemoved(entity);
						physics.bodyTypeParams[0] = fly.size.scale * HardCoded.PARAMS_FLY_WIDTHHALF;
						physics.bodyTypeParams[1] = fly.size.scale * HardCoded.PARAMS_FLY_HEIGHTHALF;
						ComponentMappers.ANIMATED.get(entity).scale.set(fly.size.scale, fly.size.scale);
						physicsSystem.entityAdded(entity);

						if (!fly.size.canGrow()) {
							// Spawn ladder
							fly.spawnLadderCountDown = HardCoded.PARAMS_FLY_LADDERCOUNTDOWN;
						} else if (fly.ladder != null) {
							// remove ladder
							getEngine().removeEntity(fly.ladder);
						}
					}
				}
			}
		}

		/*
		 * Position stuff
		 */

		// Rotate the body
		v.set(physics.body.getPosition());
		Vector2 pivotPoint = ComponentMappers.PHYSICS_COMPONENT.get(fly.pivotPoint).body.getPosition();
		v.sub(pivotPoint);

		float angle = (v.angle() - 90f) * MathUtils.degreesToRadians;
		float angleDown = angle;
		angle -= MathUtils.PI;
		v.set(1f, 1f).setAngleRad(angle).setLength(1f);

		// Move counterclockwise and alternate height (with a little bit of
		// randomness)
		float distance = v.set(physics.body.getPosition()).sub(pivotPoint).len();
		// System.out.println("=========================");
		if (fly.altitudeForce.isZero())
			fly.altitudeForce.set(1f, 1f);

		if (distance > HardCoded.PARAMS_FLY_RADIUS + HardCoded.PARAMS_FLY_ALTITUTE_AMPL) {
			angle += MathUtils.PI2 * 0.1f;
			// System.out.println("TOO HIGH");
		} else if (distance < HardCoded.PARAMS_FLY_RADIUS - HardCoded.PARAMS_FLY_ALTITUTE_AMPL) {
			angle -= MathUtils.PI2 * 0.1f;
			// System.out.println("TOO LOW");
		} else {
			// angle += MathUtils.random(MathUtils.PI, MathUtils.PI2);
			// System.out.println("TOO RIGHT");
		}
		fly.altitudeForce.setAngleRad(angle).setLength(42f);

		if (fly.size.canGrow()) {
			physics.body.setLinearVelocity(fly.altitudeForce);
		}
		/*
		 * "gravity"
		 */
		v.set(physics.body.getPosition());
		v.sub(pivotPoint);
		v.scl(-distance);
		v.scl(.01f);

		// System.out.println(fly.altitudeForce);
		// System.out.println(distance);
		// System.out.println((angle * MathUtils.radiansToDegrees + 360f) %
		// 360f);

		physics.body.setTransform(physics.body.getPosition(), angleDown);

		// Ladder spawn
		if (!fly.ladderSpawned && !fly.size.canGrow() && fly.spawnLadderCountDown <= 0f) {

			Entity ropeEntity = new Entity();
			{
				PhysicsComponentRope r = new PhysicsComponentRope();
				r.flyToFollow = entity;
				ropeEntity.add(r);
				ropeEntity.add(new GravityTarget(false));
			}
			getEngine().addEntity(ropeEntity);
			fly.ladder = ropeEntity;

			fly.ladderSpawned = true;
			jam.sfx.playSfx(Sfx.FLY_FANFARE, false);
		}
		fly.spawnLadderCountDown -= deltaTime;
	}

}
