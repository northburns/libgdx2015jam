package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.viewport.Viewport;

import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.TextureAtlasConstants;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;
import northburns._2bc.jam.resourcehelp.JamResources;

/**
 * XXX: This makes a lot of assumptions, and uses some batch from another
 * system. Hackety hack-hack!
 *
 */
public class RopeRenderSystem extends IteratingSystem {

	@SuppressWarnings("unused")
	private Contacts contacts;

	@SuppressWarnings("unused")
	private JamResources jam;

	/**
	 * XXX: I dislike couplign these two systems like this.
	 */
	@SuppressWarnings("unused")
	private PhysicsSystem physicsSystem;

	@SuppressWarnings("unused")
	private Viewport viewport;

	private AnimationRenderSystem animSystem;

	public RopeRenderSystem(PhysicsSystem physicsSystem, Contacts contact, JamResources jam, Viewport viewport,
			AnimationRenderSystem animSystem, int priority) {
		super(Family.all(PhysicsComponentRope.class).get(), priority);
		this.physicsSystem = physicsSystem;
		contacts = contact;
		this.jam = jam;
		this.viewport = viewport;
		this.animSystem = animSystem;
		/*
		 * XXX: ...
		 */
		step = jam.assets.get(TextureAtlasConstants.FLY.assetDescriptor).findRegion("tikas");
	}

	/**
	 * A temp vector used for calcs. No need to pollute the heap.
	 */
	private Vector2 v = new Vector2();

	private AtlasRegion step;

	@Override
	public void update(float deltaTime) {
		animSystem.batch.begin();
		super.update(deltaTime);
		animSystem.batch.end();
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PhysicsComponentRope rope = ComponentMappers.PHYSICS_COMPONENT_ROPE.get(entity);

		for (Body b : rope.physics.bodies) {
			v.set(b.getPosition()).sub(HardCoded.STEP_HALFWIDTH, HardCoded.STEP_HALFHEIGHT);
			float x = v.x;
			float y = v.y;
			float originX = HardCoded.STEP_HALFWIDTH;
			float originY = HardCoded.STEP_HALFHEIGHT;
			float width = 2 * HardCoded.STEP_HALFWIDTH;
			float height = 2 * HardCoded.STEP_HALFHEIGHT;
			float scaleX = 1f;
			float scaleY = 1f;
			float rotation = b.getAngle() * MathUtils.radiansToDegrees;
			animSystem.batch.draw(step, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
		}
	}

}
