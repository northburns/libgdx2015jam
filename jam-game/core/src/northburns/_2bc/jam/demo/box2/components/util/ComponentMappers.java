package northburns._2bc.jam.demo.box2.components.util;

import com.badlogic.ashley.core.ComponentMapper;

import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.AnimationControl;
import northburns._2bc.jam.demo.box2.components.CameraComponent;
import northburns._2bc.jam.demo.box2.components.FlyData;
import northburns._2bc.jam.demo.box2.components.FruitData;
import northburns._2bc.jam.demo.box2.components.GravitySource;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope;
import northburns._2bc.jam.demo.box2.components.PlanetComponent;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;

public final class ComponentMappers {
	private ComponentMappers() {
	}

	public static final ComponentMapper<Animated> ANIMATED = ComponentMapper.getFor(Animated.class);
	public static final ComponentMapper<PlayerControlled> PLAYER_CONTROLLED = ComponentMapper
			.getFor(PlayerControlled.class);
	public static final ComponentMapper<PhysicsComponent> PHYSICS_COMPONENT = ComponentMapper
			.getFor(PhysicsComponent.class);
	public static final ComponentMapper<GravityTarget> GRAVITY_TARGET = ComponentMapper.getFor(GravityTarget.class);
	public static final ComponentMapper<GravitySource> GRAVITY_SOURCE = ComponentMapper.getFor(GravitySource.class);

	public static final ComponentMapper<AnimationControl> ANIMATION_CONTROL = ComponentMapper
			.getFor(AnimationControl.class);

	public static final ComponentMapper<CameraComponent> CAMERA = ComponentMapper.getFor(CameraComponent.class);
	public static final ComponentMapper<FruitData> FRUIT_DATA = ComponentMapper.getFor(FruitData.class);
	public static final ComponentMapper<FlyData> FLY_DATA = ComponentMapper.getFor(FlyData.class);
	public static final ComponentMapper<PhysicsComponentRope> PHYSICS_COMPONENT_ROPE = ComponentMapper
			.getFor(PhysicsComponentRope.class);
	public static final ComponentMapper<PlanetComponent> PLANET = ComponentMapper.getFor(PlanetComponent.class);
}
