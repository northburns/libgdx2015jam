package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;

public class FlyData implements Component {

	/**
	 * Must have a {@link PhysicsComponent}.
	 */
	public Entity pivotPoint;
	/**
	 * The "original" distance from the pivot point.
	 */
	public float distanceZero;

	/**
	 * Distance from the pivot point
	 */
	public float distance;
	/**
	 * The current
	 */
	public float angle;

	public float altitudeChangeAmplitude;
	public float altitudeChangeFrequency;

	/**
	 * The angle changes with time, this controls the speed.
	 */
	public float angleDeltaCoefficient;

	public Size size = Size.S;
	public Vector2 altitudeForce = new Vector2(1f, 1f);
	public Entity ladder;
	public float spawnLadderCountDown;
	public boolean ladderSpawned;

	public enum Size {

		XS(.5f), S(.75f), M(1f), L(2f), XL(3f), XXL(4f), XXXL(5f);
		public final float scale;

		private Size(float scale) {
			this.scale = scale;
		}

		public Size getNext(boolean bigger) {
			switch (this) {
			case L:
				return bigger ? XL : M;
			case M:
				return bigger ? L : S;
			case S:
				return bigger ? M : XS;
			case XL:
				return bigger ? XXL : L;
			case XS:
				return bigger ? S : null;
			case XXL:
				return bigger ? XXXL : XL;
			case XXXL:
				return bigger ? null : XXL;
			default:
				throw new IllegalArgumentException("Unkown size: " + this);
			}
		}

		public boolean canGrow() {
			return getNext(true) != null;
		}

		public boolean canShrink() {
			return getNext(false) != null;
		}
	}

}
