package northburns._2bc.jam.demo.box2.data.animated;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import java.util.Objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

public class AnimationDetails {

	public float frameDuration;
	public Array<KeyFrameDetails> keys;
	public String mode;
	public boolean useGlobalDrawAndWorldValues;
	public int drawZeroX, drawZeroY;
	public float worldWidth, worldHeight;

	public AnimationDetails() {
		this(1f);
	}

	public AnimationDetails(float frameDuration) {
		this.frameDuration = frameDuration;
		this.mode = "loop";
	}

	private static Json fromJson;
	public Animation animation;
	public boolean dontInterrupt = false;

	public static AnimationDetails fromJson(String jsonData) {
		if (null == fromJson)
			fromJson = new Json();
		/*
		 * I read this: https://github.com/libgdx/libgdx/wiki/Reflection But
		 * still couldn't get this to work on GWT. Wierd.. Well, doing it the
		 * hard way.
		 */
		// return fromJson.fromJson(AnimationDetails.class, jsonData);
		/*
		 * TODO: Read above and fix that.
		 */
		AnimationDetails result = new AnimationDetails();
		JsonReader read = new JsonReader();
		JsonValue parse = read.parse(jsonData);
		result.frameDuration = parse.getFloat("frameDuration");
		result.mode = parse.getString("mode");
		result.useGlobalDrawAndWorldValues = parse.getBoolean("useGlobalDrawAndWorldValues");
		result.drawZeroX = parse.getInt("drawZeroX");
		result.drawZeroY = parse.getInt("drawZeroY");
		result.worldWidth = parse.getFloat("worldWidth");
		result.worldHeight = parse.getFloat("worldHeight");
		result.keys = new Array<>();
		for (JsonValue entry = parse.getChild("keys"); entry != null; entry = entry.next) {
			KeyFrameDetails key = new KeyFrameDetails();
			key.frame = entry.getString("frame");
			key.drawZeroX = parse.getInt("drawZeroX");
			key.drawZeroY = parse.getInt("drawZeroY");
			key.worldWidth = parse.getFloat("worldWidth");
			key.worldHeight = parse.getFloat("worldHeight");
			result.keys.add(key);
		}
		return result;
	}

	/**
	 * AnimationDetails json demo
	 */
	public static void main(String... args) {
		AnimationDetails ad = new AnimationDetails(1f);
		ad.keys.add(new KeyFrameDetails("framename", 10, 20, 30.5f, 40.5f));
		Json json = new Json(OutputType.json);
		System.out.println(json.prettyPrint(ad));
	}

	public Array<TextureRegion> frameNames(TextureAtlas atlas) {
		Array<TextureRegion> result = new Array<>(keys.size);
		for (KeyFrameDetails d : keys)
			result.add(Objects.requireNonNull(atlas.findRegion(d.frame), "No region found for " + d.frame));
		return result;
	}

	public PlayMode playMode() {
		if ("loop".equals(mode))
			return PlayMode.LOOP;
		if ("normal".equals(mode))
			return PlayMode.NORMAL;
		throw new IllegalArgumentException("Unsupported playmode: " + mode);
	}

	public void initAnimation(TextureAtlas atlas) {
		animation = new Animation(frameDuration, frameNames(atlas), playMode());
	}

}
