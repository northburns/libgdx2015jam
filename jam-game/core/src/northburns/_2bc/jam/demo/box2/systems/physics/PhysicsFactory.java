package northburns._2bc.jam.demo.box2.systems.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.utils.Array;

import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope.BodyForRope;

final class PhysicsFactory {
	private PhysicsFactory() {
	}

	/**
	 * For "simple" bodies with one main fixture, and one (optional)
	 * sensorFixture.
	 *
	 */
	public static class BodyWithSensorFixture {

		private BodyWithSensorFixture(Body body, Fixture mainFixture, Fixture sensorFixture) {
			this.body = body;
			this.mainFixture = mainFixture;
			this.sensorFixture = sensorFixture;
		}

		public final Body body;
		public final Fixture mainFixture;
		public final Fixture sensorFixture;
	}

	static BodyWithSensorFixture createPlayerBody(World world, Vector2 pos, float width,
			float heightSansSensorFixture) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(pos);
		bodyDef.fixedRotation = true;

		Body body = world.createBody(bodyDef);

		PolygonShape poly = new PolygonShape();

		poly.setAsBox(width, heightSansSensorFixture);
		Fixture playerPhysicsFixture = body.createFixture(poly, 1);

		poly.dispose();

		CircleShape circle = new CircleShape();
		circle.setRadius(width * .1f);
		circle.setPosition(new Vector2(0, -heightSansSensorFixture));
		Fixture playerSensorFixture = body.createFixture(circle, 0);
		playerSensorFixture.setSensor(true);

		circle.dispose();

		body.setBullet(true);
		return new BodyWithSensorFixture(body, playerPhysicsFixture, playerSensorFixture);
	}

	static BodyWithSensorFixture gravityPlanet(World world, Vector2 pos, float rPlanet, float rGravity) {
		final Body moonBody;
		BodyDef groundBodyDef = new BodyDef();
		groundBodyDef.type = BodyType.StaticBody;
		groundBodyDef.position.set(pos);
		moonBody = world.createBody(groundBodyDef);

		CircleShape shape = new CircleShape();
		shape.setRadius(rPlanet);
		Fixture moonFixture = moonBody.createFixture(shape, 0f);
		shape.dispose();

		CircleShape circle = new CircleShape();
		circle.setRadius(rGravity);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.isSensor = true;

		Fixture gravity = moonBody.createFixture(fixtureDef);
		circle.dispose();

		return new BodyWithSensorFixture(moonBody, moonFixture, gravity);
	}

	static BodyWithSensorFixture createFruit(World world, Vector2 pos, float radius, float velocityX, float velocityY,
			float angularVelocity) {
		final Body fruitBody;
		BodyDef groundBodyDef = new BodyDef();
		groundBodyDef.type = BodyType.DynamicBody;
		groundBodyDef.position.set(pos);
		fruitBody = world.createBody(groundBodyDef);

		CircleShape shape = new CircleShape();
		shape.setRadius(radius);
		Fixture fruitFixture = fruitBody.createFixture(shape, HardCoded.PARAMS_FRUIT_DENSITY);
		shape.dispose();

		fruitFixture.setRestitution(HardCoded.PARAMS_FRUIT_RESTITUTIONCOEFFICIENT);

		fruitBody.setLinearVelocity(velocityX, velocityY);
		fruitBody.setAngularVelocity(angularVelocity);

		fruitBody.applyLinearImpulse(velocityX, velocityY, pos.x, pos.y, true);

		return new BodyWithSensorFixture(fruitBody, fruitFixture, null);
	}

	static BodyWithSensorFixture createFly(World world, Vector2 pos, float halfWidth, float halfHeight) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.KinematicBody;
		bodyDef.position.set(pos);
		bodyDef.fixedRotation = true;

		Body body = world.createBody(bodyDef);

		/*
		 * TODO: as Ellipse!
		 */
		PolygonShape poly = new PolygonShape();
		poly.setAsBox(halfWidth, halfHeight);
		Fixture playerPhysicsFixture = body.createFixture(poly, 0);
		// playerPhysicsFixture.setSensor(true);
		poly.dispose();

		body.setBullet(true);
		return new BodyWithSensorFixture(body, playerPhysicsFixture, null);

	}

	private static final Vector2 v = new Vector2();

	static BodyForRope createRope(World world, Vector2 pos, Body flyBody) {
		/*
		 * XXX: Some constants here.
		 */

		final int STEPS = 10;

		BodyForRope body = new BodyForRope();
		body.joints = new Array<>((STEPS - 1) * 2);
		body.bodies = new Array<>(STEPS);

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;

		PolygonShape poly = new PolygonShape();
		poly.setAsBox(HardCoded.STEP_HALFWIDTH, HardCoded.STEP_HALFHEIGHT);

		v.set(pos);
		float angle = flyBody.getAngle();

		Body stepPrev = null;
		for (int i = 0; i < STEPS; ++i) {
			bodyDef.position.set(v /* .sub(0, HardCoded.STEP_HALFHEIGHT * 4) */);
			bodyDef.angle = angle;
			Body step = world.createBody(bodyDef);
			body.bodies.add(step);

			Fixture stepFixture = step.createFixture(poly, 1000f);
			stepFixture.setSensor(true);

			if (stepPrev == null) {
				body.topStep = step;
				// // Fix the top to the fly
				stepPrev = flyBody;
				// // With just one joint
				// RopeJointDef ropeJointDef = new RopeJointDef();
				//
				// ropeJointDef.bodyA = step;
				// ropeJointDef.bodyB = stepPrev;
				//
				// ropeJointDef.localAnchorA.set(0f, 0f);
				// ropeJointDef.localAnchorB.set(0f, 0f);
				//
				// ropeJointDef.collideConnected = false;
				// // ropeJointDef.dampingRatio = 1;
				// ropeJointDef.maxLength = HardCoded.ROPE_MAX_LENGTH;
				//
				// body.joints.add(world.createJoint(ropeJointDef));
			}
			{
				RopeJointDef weldJointDef = new RopeJointDef();

				weldJointDef.bodyA = step;
				weldJointDef.bodyB = stepPrev;

				weldJointDef.localAnchorA.set(HardCoded.STEP_HALFWIDTH - .5f, 0f);
				weldJointDef.localAnchorB.set(HardCoded.STEP_HALFWIDTH - .5f, 0f);

				weldJointDef.collideConnected = false;
				// weldJointDef.dampingRatio = 1;
				weldJointDef.maxLength = HardCoded.ROPE_MAX_LENGTH;

				body.joints.add(world.createJoint(weldJointDef));

				weldJointDef.localAnchorA.scl(-1f, -1f);
				weldJointDef.localAnchorB.scl(-1f, -1f);
				body.joints.add(world.createJoint(weldJointDef));
			}
			stepPrev = step;
		}

		poly.dispose();

		return body;
	}

}
