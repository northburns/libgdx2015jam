package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

import northburns._2bc.jam.demo.box2.data.animated.AnimationDetails;

public class Animated implements Component {
	public AnimationDetails animation;

	public ObjectMap<String, AnimationDetails> animationStorage;
	private String queuedAnimation;

	public float stateTime;
	public float rotation;
	public Vector2 pos, scale;
	/**
	 * The animation system draws animated in layers. Each layer may have its
	 * own shader.
	 */
	public int layer;
	/**
	 * False is "normal". True means that the animated's rotation is set from
	 * camera (ie. fixed to the viewport).
	 */
	public boolean rotateFromCamera;
	public Vector2 posToAnimOrigin;

	private String currentAnimationKey;

	private Runnable queuedWhenFinished;

	private Runnable whenFinished;

	public Animated(AnimationDetails currentAnimation, ObjectMap<String, AnimationDetails> animationStorage,
			int layer) {
		this.animation = currentAnimation;
		this.animationStorage = animationStorage;
		this.layer = layer;
		pos = new Vector2();
		scale = new Vector2(1f, 1f);
		posToAnimOrigin = new Vector2();
	}

	public void changeAnimation(String key) {
		changeAnimation(key, null);
	}

	public void changeAnimation(String key, Runnable whenFinished) {
		if (key.equals(currentAnimationKey))
			return;
		if (!animationStorage.containsKey(key))
			throw new IllegalArgumentException("Key not in anim storage: " + key);
		if (animation.dontInterrupt) {
			queuedAnimation = key; // replace previous
									// (even if that is
									// "dontInterrupt"!)
			queuedWhenFinished = whenFinished;
		} else {
			animation = animationStorage.get(key);
			currentAnimationKey = key;
			stateTime = 0f;
			this.whenFinished = whenFinished;
		}

	}

	public void update(float deltaTime) {
		stateTime += deltaTime;
		boolean isCurrentFinished = animation.animation.isAnimationFinished(stateTime);
		if (whenFinished != null && isCurrentFinished) {
			whenFinished.run();
			whenFinished = null;
		}
		if (queuedAnimation != null && isCurrentFinished) {
			animation = animationStorage.get(queuedAnimation);
			whenFinished = queuedWhenFinished;
			currentAnimationKey = queuedAnimation;
			stateTime = 0f;
			queuedAnimation = null;
			queuedWhenFinished = null;
		}
	}
}
