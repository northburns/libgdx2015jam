package northburns._2bc.jam.demo.box2.systems;

import java.util.Comparator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.Viewport;

import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.AnimationControl;
import northburns._2bc.jam.demo.box2.components.CameraComponent;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.data.animated.KeyFrameDetails;
import northburns._2bc.jam.resourcehelp.JamResources;

/**
 * Implementing Disposable.. I hope that the user of this class notices that :D
 *
 */
public class AnimationRenderSystem extends SortedIteratingSystem implements Disposable {

	private static final Family FAMILY_ANIMCONTROL = Family.all(AnimationControl.class).get();
	private static final Family FAMILY_CAMERA = Family.all(CameraComponent.class).get();
	final SpriteBatch batch;
	@SuppressWarnings("unused")
	private AssetManager assets;

	/**
	 * Temp Vector3s
	 */
	private final Vector3 v3a = new Vector3(), v3b = new Vector3();

	private Box2DDebugRenderer debugRenderer;
	private World box2dWorldToDebug;
	@SuppressWarnings("unused")
	private JamResources jamResources;

	private static final Comparator<Entity> ENTITY_COMPARATOR = new Comparator<Entity>() {

		@Override
		public int compare(Entity o1, Entity o2) {
			Animated a1 = ComponentMappers.ANIMATED.get(o1);
			Animated a2 = ComponentMappers.ANIMATED.get(o2);
			return a1.layer - a2.layer;
		}

	};
	private Viewport viewport;

	public AnimationRenderSystem(AssetManager assets, JamResources jamResources, Viewport viewport,
			World box2dWorldToDebug, int priority) {
		super(Family.all(Animated.class).get(), ENTITY_COMPARATOR, priority);
		this.assets = assets;
		this.jamResources = jamResources;
		this.viewport = viewport;
		this.batch = new SpriteBatch();
		this.box2dWorldToDebug = box2dWorldToDebug;

		debugRenderer = new Box2DDebugRenderer();
		debugRenderer.setDrawVelocities(false);
		debugRenderer.setDrawBodies(true);
		debugRenderer.setDrawAABBs(false);

	}

	private AnimationControl animationControl;
	private int lastDrawnLayer;
	private CameraComponent camera;

	@Override
	public void update(float deltaTime) {

		ImmutableArray<Entity> cameraEntities = getEngine().getEntitiesFor(FAMILY_CAMERA);
		if (cameraEntities.size() != 1)
			throw new IllegalStateException("Number of Camera entities was not 1.");
		this.camera = ComponentMappers.CAMERA.get(cameraEntities.first());

		ImmutableArray<Entity> animationControlEntities = getEngine().getEntitiesFor(FAMILY_ANIMCONTROL);
		if (animationControlEntities.size() != 1)
			throw new IllegalStateException("Number of AnimationControl entities was not 1.");
		animationControl = ComponentMappers.ANIMATION_CONTROL.get(animationControlEntities.first());

		// camera.update(); that's handled by the camera system, right?

		viewport.apply();
		batch.begin();

		batch.setProjectionMatrix(camera.camera.combined);

		lastDrawnLayer = -1;
		super.update(deltaTime);

		drawDebug();

		batch.end();
	}

	private void drawDebug() {
		if (animationControl.renderDebug) {
			batch.end();
			this.debugRenderer.render(box2dWorldToDebug, camera.camera.combined);
			batch.begin();
		}
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {

		Animated anim = ComponentMappers.ANIMATED.get(entity);
		int layer = anim.layer;
		anim.update(deltaTime);

		// KeyFrame
		TextureRegion keyFrame = anim.animation.animation.getKeyFrame(anim.stateTime);
		KeyFrameDetails d = anim.animation.keys.get(anim.animation.animation.getKeyFrameIndex(anim.stateTime));

		// Position, rotation, etc
		float rotation = -anim.rotation;

		if (anim.rotateFromCamera)
			rotation += camera.lastSetRotation * MathUtils.degreesToRadians;
		Vector2 pos = anim.pos, scale = anim.scale;

		int drawZeroX, drawZeroY;
		float worldWidth, worldHeight;
		if (anim.animation.useGlobalDrawAndWorldValues) {
			drawZeroX = anim.animation.drawZeroX;
			drawZeroY = anim.animation.drawZeroY;
			worldWidth = anim.animation.worldWidth;
			worldHeight = anim.animation.worldHeight;
		} else {
			drawZeroX = d.drawZeroX;
			drawZeroY = d.drawZeroY;
			worldWidth = d.worldWidth;
			worldHeight = d.worldHeight;
		}

		v3a.set(pos.x, pos.y, 0f);
		v3b.set(drawZeroX, keyFrame.getRegionHeight() - drawZeroY, 0f)
				.scl(1f / keyFrame.getRegionWidth(), 1f / keyFrame.getRegionHeight(), 0f)
				.scl(worldWidth, worldHeight, 0f);
		v3b.add(anim.posToAnimOrigin.x, anim.posToAnimOrigin.y, 0f);
		v3a.sub(v3b);

		// Shader
		if (lastDrawnLayer != layer) {
			// layer changed, change shader
			if (animationControl.layerShaders.containsKey(layer)) {
				ShaderProgram shader = animationControl.layerShaders.get(layer);
				batch.setShader(shader);
				animationControl.layerShaderAppliers.get(layer).apply(deltaTime, shader);
			} else {
				batch.setShader(null);
			}
		}

		batch.draw(keyFrame, v3a.x, v3a.y, v3b.x, v3b.y, worldWidth, worldHeight, scale.x, scale.y,
				-(rotation * MathUtils.radiansToDegrees));

		lastDrawnLayer = layer;
	}

	@Override
	public void dispose() {
		batch.dispose();
		debugRenderer.dispose();
	}
}
