package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;

public class PlayerControlled implements Component {

	public enum InputCode {
		WALK_LEFT, WALK_RIGHT, JUMP, THROW, DEBUG_TOGGLE_BOX2D, DEBUG_INSTANTWIN

	}

	public Input input = new Input();
	public Output output = new Output();
	public boolean hasLanded = false;

	public class Input {
		private Input() {
		}

		public boolean walkLeft;
		public boolean walkRight;

		public float secondsNotWalking;

		public boolean jump;
		public boolean thrw;

		public boolean debug_toggleBox2d;

	}

	public class Output {
		private Output() {
		}

		public float secondsSinceLastThrow = Float.MAX_VALUE;
	}

}
