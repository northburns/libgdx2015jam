package northburns._2bc.jam.demo.box2.systems.physics;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactFilter;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import northburns._2bc.jam.demo.box2.components.GravitySource;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope.BodyForRope;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsFactory.BodyWithSensorFixture;
import northburns._2bc.jam.demo.box2.util.GravityUtil;
import northburns._2bc.jam.util.Optional;

public class PhysicsSystem extends EntitySystem implements EntityListener, Disposable, ContactListener, ContactFilter {

	public static final Family family = Family.one(PhysicsComponent.class, PhysicsComponentRope.class).get();

	private static final ComponentMapper<PhysicsComponent> cm = ComponentMapper.getFor(PhysicsComponent.class);

	public World world;
	public Contacts contacts = new Contacts();

	public PhysicsSystem(int priority) {
		super(priority);
		world = new World(new Vector2(0, 0), true);
		world.setContactListener(this);
		world.setContactFilter(this);
	}

	@Override
	public void entityAdded(Entity entity) {
		PhysicsComponent c = ComponentMappers.PHYSICS_COMPONENT.get(entity);
		if (c != null) {
			BodyWithSensorFixture body;
			switch (c.bodyType) {
			case PLANET:
				body = PhysicsFactory.gravityPlanet(world, c.creationPoint, c.bodyTypeParams[0], c.bodyTypeParams[1]);
				break;
			case PLAYER:
				body = PhysicsFactory.createPlayerBody(world, c.creationPoint, c.bodyTypeParams[0],
						c.bodyTypeParams[1]);
				break;
			case FRUIT:
				body = PhysicsFactory.createFruit(world, c.creationPoint, c.bodyTypeParams[0], c.bodyTypeParams[1],
						c.bodyTypeParams[2], c.bodyTypeParams[3]);
				break;
			case FLY:
				body = PhysicsFactory.createFly(world, c.creationPoint, c.bodyTypeParams[0], c.bodyTypeParams[1]);
				break;
			default:
				throw new IllegalArgumentException("Unkown body type: " + c.bodyType);
			}
			c.body = body.body;
			c.bodyFixture = body.mainFixture;
			c.sensorFixture = Optional.fromNullable(body.sensorFixture);

			c.body.setUserData(new PhysicsUserData(entity));
		}
		PhysicsComponentRope r = ComponentMappers.PHYSICS_COMPONENT_ROPE.get(entity);
		if (r != null) {
			Body flyBody = ComponentMappers.PHYSICS_COMPONENT.get(r.flyToFollow).body;
			BodyForRope forRope = PhysicsFactory.createRope(world, flyBody.getPosition(), flyBody);

			r.physics = forRope;

			for (Body b : r.physics.bodies)
				b.setUserData(new PhysicsUserData(entity));
		}
	}

	@Override
	public void entityRemoved(Entity entity) {
		PhysicsComponent c = cm.get(entity);
		if (c != null)
			world.destroyBody(c.body);
		PhysicsComponentRope r = ComponentMappers.PHYSICS_COMPONENT_ROPE.get(entity);
		if (r != null) {
			for (Body b : r.physics.bodies)
				world.destroyBody(b); // destroys joints as well, ok..
		}
	}

	@Override
	public void update(float deltaTime) {
		/*
		 * XXX:
		 * https://github.com/libgdx/libgdx/wiki/Box2d#stepping-the-simulation
		 */
		world.step(deltaTime, 6, 2);
	}

	@Override
	public void dispose() {
		world.dispose();
		world = null;
	}

	public static class Contacts {
		/**
		 * Keys are entity keys for gravity targets. Each target has an array of
		 * gravity sources it has a contact with.
		 */
		public ObjectMap<Entity, Array<Entity>> gravityTargetsSources = new ObjectMap<>();
		public int playerSensorContactCount = 0;
		public ObjectMap<Entity, Array<Entity>> flyTouchesFruit = new ObjectMap<>();
		public int playerSensorToLaddersCount = 0;

		private Array<Entity> getOrCreate(Entity target, ObjectMap<Entity, Array<Entity>> from) {
			if (!from.containsKey(target))
				from.put(target, new Array<Entity>());
			return from.get(target);
		}
	}

	@Override
	public void beginContact(Contact contact) {
		/*
		 * Gravity pairs
		 */
		GravitySourceAndTarget gravity = getIfGravitySourceAndTargetPair(contact);
		if (gravity != null) {
			contacts.getOrCreate(gravity.gravityTarget, contacts.gravityTargetsSources).add(gravity.gravitySource);
		}
		/*
		 * Fruit to fly
		 */
		EntityPair ep = getIfContactIsOfTypes(contact, ComponentMappers.FLY_DATA, ComponentMappers.FRUIT_DATA);
		if (ep != null) {
			contacts.getOrCreate(ep.e1, contacts.flyTouchesFruit).add(ep.e2);
		}
		/*
		 * Player sensor contact count
		 */
		Fixture contactFixture = GravityUtil.ifIsPlayerControlledSensorFixtureTouching(contact);
		if (contactFixture != null) {
			if (!contactFixture.isSensor()) {
				contacts.playerSensorContactCount++;
			}
			if (ComponentMappers.PHYSICS_COMPONENT_ROPE.has(GravityUtil.getFixturesBodysEntity(contactFixture))) {
				contacts.playerSensorToLaddersCount++;
			}
		}
	}

	@Override
	public void endContact(Contact contact) {
		/*
		 * Gravity pairs
		 */
		GravitySourceAndTarget gravity = getIfGravitySourceAndTargetPair(contact);
		if (gravity != null) {
			contacts.getOrCreate(gravity.gravityTarget, contacts.gravityTargetsSources)
					.removeValue(gravity.gravitySource, true);
		}
		/*
		 * Fruit to fly
		 */
		EntityPair ep = getIfContactIsOfTypes(contact, ComponentMappers.FLY_DATA, ComponentMappers.FRUIT_DATA);
		if (ep != null) {
			contacts.getOrCreate(ep.e1, contacts.flyTouchesFruit).removeValue(ep.e2, true);
		}
		/*
		 * Player sensor contact count
		 */
		Fixture contactFixture = GravityUtil.ifIsPlayerControlledSensorFixtureTouching(contact);
		if (contactFixture != null) {
			if (!contactFixture.isSensor()) {
				contacts.playerSensorContactCount--;
			}
			if (ComponentMappers.PHYSICS_COMPONENT_ROPE.has(GravityUtil.getFixturesBodysEntity(contactFixture))) {
				contacts.playerSensorToLaddersCount--;
			}
		}
	}

	private static class EntityPair {
		Entity e1;
		Entity e2;

		private EntityPair set(Entity e1, Entity e2) {
			this.e1 = e1;
			this.e2 = e2;
			return this;
		}
	}

	private static final EntityPair typepair = new EntityPair();

	/**
	 * 
	 * @param contact
	 * @param m1
	 * @param m2
	 * @return e1 matches m1, and e2 matches m2.
	 */
	private static <T1 extends Component, T2 extends Component> EntityPair getIfContactIsOfTypes(Contact contact,
			ComponentMapper<T1> m1, ComponentMapper<T2> m2) {
		Entity entityA = GravityUtil.getFixturesBodysEntity(contact.getFixtureA());
		Entity entityB = GravityUtil.getFixturesBodysEntity(contact.getFixtureB());

		/*
		 * TODO: if both of those if-statements were true...
		 */
		if (m1.has(entityA) && m2.has(entityB)) {
			return typepair.set(entityA, entityB);
		}
		if (m1.has(entityB) && m2.has(entityA)) {
			return typepair.set(entityB, entityA);
		}
		return null;
	}

	@Deprecated
	public static class GravitySourceAndTarget {
		public Entity gravitySource;
		public Entity gravityTarget;

		private GravitySourceAndTarget set(Entity gravitySource, Entity gravityTarget) {
			this.gravitySource = gravitySource;
			this.gravityTarget = gravityTarget;
			return this;
		}
	}

	@Deprecated
	private static final GravitySourceAndTarget gravitySourceAndTarget = new GravitySourceAndTarget();

	/**
	 * Always returns the same instance!
	 * 
	 * @deprecated Use {@link #getIfContactIsOfTypes} instead.
	 */
	@Deprecated
	public static GravitySourceAndTarget getIfGravitySourceAndTargetPair(Contact contact) {
		Entity entityA = GravityUtil.getFixturesBodysEntity(contact.getFixtureA());
		Entity entityB = GravityUtil.getFixturesBodysEntity(contact.getFixtureB());

		ComponentMapper<GravitySource> cmSource = ComponentMappers.GRAVITY_SOURCE;
		ComponentMapper<GravityTarget> cmTarget = ComponentMappers.GRAVITY_TARGET;

		if (cmSource.has(entityA) && cmSource.has(entityB)) {
			throw new UnsupportedOperationException("Both were gravity sources. Currently unsupported.");
			// Note: might have to return two gravitysource'n'target pairs.
		}
		if (!cmSource.has(entityA) && !cmSource.has(entityB)) {
			// neither is a source
			return null;
		}
		if (!cmTarget.has(entityA) && !cmTarget.has(entityB)) {
			// neither is a target
			return null;
		}
		Entity source = cmSource.has(entityA) ? entityA : entityB;
		Entity target = cmTarget.has(entityA) ? entityA : entityB;
		return gravitySourceAndTarget.set(source, target);

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// NOP
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// NOP
	}

	@Override
	public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {
		Entity entityA = GravityUtil.getFixturesBodysEntity(fixtureA);
		Entity entityB = GravityUtil.getFixturesBodysEntity(fixtureB);

		if (ComponentMappers.PLAYER_CONTROLLED.has(entityA) ^ ComponentMappers.PLAYER_CONTROLLED.has(entityB)) {
			// One of them is a player controlled
			if (ComponentMappers.FRUIT_DATA.has(entityA) ^ ComponentMappers.FRUIT_DATA.has(entityB)) {
				// One of them is a fruit
				return false; // XXX: What the same one is both a player and a
								// fruit!
			}
			if (ComponentMappers.FLY_DATA.has(entityA) ^ ComponentMappers.FLY_DATA.has(entityB)) {
				// One of them is a fly
				return false; // XXX: What the same one is both a player and a
								// fly!
			}
		}

		return true;
	}
}
