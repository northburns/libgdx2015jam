package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;

/**
 * The entity ought to have also an {@link PhysicsComponent}, to read the
 * position from.
 *
 */
public class GravitySource implements Component {

	public float force;

	public GravitySource(float force) {
		this.force = force;
	}

}
