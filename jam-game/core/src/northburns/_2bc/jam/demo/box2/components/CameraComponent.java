package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class CameraComponent implements Component {
	public OrthographicCamera camera;
	public float lastSetRotation;

	public CameraComponent(OrthographicCamera camera) {
		this.camera = camera;
	}
}
