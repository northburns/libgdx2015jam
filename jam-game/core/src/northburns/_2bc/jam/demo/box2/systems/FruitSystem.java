package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import northburns._2bc.jam.constants.FruitAnimations;
import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.FruitData;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PlanetComponent;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;
import northburns._2bc.jam.resourcehelp.JamResources;

public class FruitSystem extends IteratingSystem {

	private static final Family FAMILY_PLANET = Family.one(PlanetComponent.class).get();

	@SuppressWarnings("unused")
	private Contacts contacts;

	private JamResources jam;

	/**
	 * XXX: I dislike couplign these two systems like this.
	 */
	@SuppressWarnings("unused")
	private PhysicsSystem physicsSystem;

	private Runnable whenPlanetDies;

	private Entity planet;

	public FruitSystem(PhysicsSystem physicsSystem, Contacts contacts, JamResources jam, Runnable whenPlanetDies,
			int priority) {
		super(Family.all(FruitData.class).get(), priority);
		this.physicsSystem = physicsSystem;
		this.contacts = contacts;
		this.jam = jam;
		this.whenPlanetDies = whenPlanetDies;
	}

	/**
	 * A temp vector used for calcs. No need to pollute the heap.
	 */
	private Vector2 v = new Vector2();

	@Override
	public void update(float deltaTime) {
		planet = getEngine().getEntitiesFor(FAMILY_PLANET).first();
		super.update(deltaTime);
	}

	@Override
	protected void processEntity(final Entity entity, float deltaTime) {

		FruitData fruit = ComponentMappers.FRUIT_DATA.get(entity);
		Animated anim = ComponentMappers.ANIMATED.get(entity);
		PhysicsComponent physics = ComponentMappers.PHYSICS_COMPONENT.get(entity);

		PlanetComponent planetC = ComponentMappers.PLANET.get(planet);
		@SuppressWarnings("unused")
		Animated planetA = ComponentMappers.ANIMATED.get(planet);

		fruit.aliveTime += deltaTime;

		/*
		 * XXX: Hardcoded numbeeers etc
		 */
		if (fruit.aliveTime > 7f) {
			if (physics != null) {
				anim.changeAnimation(FruitAnimations.PUFF.key, new Runnable() {

					@Override
					public void run() {
						getEngine().removeEntity(entity);
					}
				});
				jam.sfx.playSfx(Sfx.FRUIT_PUFF, true);
				v.set(physics.body.getPosition())
						.sub(ComponentMappers.PHYSICS_COMPONENT.get(planet).body.getPosition());
				if (v.len() <= HardCoded.PARAMS_GRAVITYFIELD_RADIUS) {
					planetC.health--;
					if (planetC.health <= 0)
						whenPlanetDies.run();
				}
				anim.rotation = v.angleRad() + MathUtils.PI * .5f;

				entity.remove(PhysicsComponent.class);
			}
		} else if (fruit.aliveTime > 3.5f) {
			fruit.edible = false;
			anim.changeAnimation(FruitAnimations.STAGE_4.key);
		} else {
			fruit.edible = true;
			anim.changeAnimation(FruitAnimations.STAGE_0.key);
		}
	}

}
