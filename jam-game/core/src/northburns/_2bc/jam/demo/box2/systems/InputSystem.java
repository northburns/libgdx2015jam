package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.utils.Predicate;

import northburns._2bc.jam.demo.box2.components.AnimationControl;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;
import northburns._2bc.jam.demo.box2.components.PlayerControlled.InputCode;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.input.InputChart;

public class InputSystem extends IteratingSystem {

	private final InputChart<InputCode> input;

	private boolean onDown_jump;
	private boolean onDown_throw;
	private boolean onDown_debugBox2d;

	private InputDebugStuff debugStuff;

	public interface InputDebugStuff {
		void winNow();
	}

	public InputSystem(InputChart<InputCode> input, InputDebugStuff debugStuff, int priority) {
		super(Family.one(PlayerControlled.class, AnimationControl.class).get(), priority);
		this.input = input;
		this.debugStuff = debugStuff;
		hookToInput();
	}

	private void hookToInput() {
		input.onDown(InputCode.JUMP, new Predicate<InputCode>() {

			@Override
			public boolean evaluate(InputCode arg0) {
				onDown_jump = true;
				return true;
			}
		});
		input.onDown(InputCode.THROW, new Predicate<InputCode>() {

			@Override
			public boolean evaluate(InputCode arg0) {
				onDown_throw = true;
				return true;
			}
		});
		input.onDown(InputCode.DEBUG_TOGGLE_BOX2D, new Predicate<InputCode>() {

			@Override
			public boolean evaluate(InputCode arg0) {
				onDown_debugBox2d = true;
				return true;
			}
		});
		input.onDown(InputCode.DEBUG_INSTANTWIN, new Predicate<InputCode>() {

			@Override
			public boolean evaluate(InputCode arg0) {
				debugStuff.winNow();
				return true;
			}
		});
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PlayerControlled playerControl = ComponentMappers.PLAYER_CONTROLLED.get(entity);
		if (playerControl != null) {
			playerControl.input.walkLeft = input.isDown(InputCode.WALK_LEFT);
			playerControl.input.walkRight = input.isDown(InputCode.WALK_RIGHT);
			playerControl.input.jump = onDown_jump;
			playerControl.input.thrw = onDown_throw;

			onDown_jump = false;
			onDown_throw = false;

			if (playerControl.input.walkLeft || playerControl.input.walkRight)
				playerControl.input.secondsNotWalking = 0f;
			else
				playerControl.input.secondsNotWalking += deltaTime;

		}
		AnimationControl animControl = ComponentMappers.ANIMATION_CONTROL.get(entity);
		if (animControl != null && onDown_debugBox2d) {
			animControl.renderDebug = !animControl.renderDebug;
			onDown_debugBox2d = false;
		}
	}

}
