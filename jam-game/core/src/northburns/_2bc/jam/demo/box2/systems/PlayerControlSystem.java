package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;
import northburns._2bc.jam.constants.FruitAnimations;
import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.PlayerAnimations;
import northburns._2bc.jam.constants.Sfx;
import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.FruitData;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent.BodyType;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.data.animated.AnimationDetails;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;
import northburns._2bc.jam.resourcehelp.JamResources;

public class PlayerControlSystem extends IteratingSystem {

	@SuppressWarnings("unused")
	private static final float MAX_VELOCITY_SQUARED = 250f;

	private Contacts contacts;

	private JamResources jam;

	private Runnable whenWon;

	private Runnable whenDriftingToSpace;

	public PlayerControlSystem(JamResources jam, Contacts contacts, Runnable whenWon, Runnable whenDriftedToSpace,
			int priority) {
		super(Family.all(PlayerControlled.class, PhysicsComponent.class, GravityTarget.class).get(), priority);
		this.jam = jam;
		this.contacts = contacts;
		this.whenWon = whenWon;
		this.whenDriftingToSpace = whenDriftedToSpace;
	}

	/**
	 * A temp vector used for calcs. No need to pollute the heap.
	 */
	private Vector2 v = new Vector2();

	/**
	 * XXX: Monolithic method...
	 */
	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PlayerControlled control = ComponentMappers.PLAYER_CONTROLLED.get(entity);
		PhysicsComponent physics = ComponentMappers.PHYSICS_COMPONENT.get(entity);
		GravityTarget gravityTarget = ComponentMappers.GRAVITY_TARGET.get(entity);
		Animated anim = ComponentMappers.ANIMATED.get(entity);

		if (gravityTarget.hasGravitySourceAtTheMoment) {
			Entity strongest = null;
			for (Entity gravitySource : contacts.gravityTargetsSources.get(entity)) {
				if (strongest == null || ComponentMappers.GRAVITY_SOURCE
						.get(gravitySource).force > ComponentMappers.GRAVITY_SOURCE.get(strongest).force)
					strongest = gravitySource;
			}

			if (strongest != null) {
				// Rotate so that bottom points to the strongest gravity
				// TODO: that.. now we just set the rotation..
				v.set(physics.body.getPosition());
				Vector2 gravitySourcePosition = ComponentMappers.PHYSICS_COMPONENT.get(strongest).body.getPosition();
				v.sub(gravitySourcePosition);
				float angle = (v.angle() - 90f) * MathUtils.degreesToRadians;
				physics.body.setTransform(physics.body.getPosition(), angle);

				gravityTarget.strongestGravitySourcePoint.set(gravitySourcePosition);
			}
		}

		/*
		 * Player's control
		 */
		boolean grounded = contacts.playerSensorContactCount > 0;
		if (grounded)
			control.hasLanded = true;
		final float walkingSpeedSquared = grounded ? HardCoded.PHYSICS_WALKINGSPEED_GROUNDED
				: HardCoded.PHYSICS_WALKINGSPEED_INAIR;

		if (control.input.walkLeft || control.input.walkRight) {
			float angle = physics.body.getAngle();
			if (control.input.walkLeft)
				angle += MathUtils.PI;
			v.setAngleRad(angle);
			v.setLength2(walkingSpeedSquared);
			physics.body.applyLinearImpulse(v, physics.body.getPosition(), true);
		}
		if (grounded && control.input.secondsNotWalking > 0.2f) {
			// XXX: Actually hardcoded value...
			/*
			 * Is on the ground and the player hasn't wanted to move for a
			 * while: halt.
			 */
			physics.bodyFixture.setFriction(HardCoded.PHYSICS_FRICTION_STOP);
		} else
			physics.bodyFixture.setFriction(HardCoded.PHYSICS_FRICTION_WALK);

		if (grounded && control.input.jump) {
			v.setAngleRad(physics.body.getAngle() + MathUtils.PI * .5f);
			v.setLength2(HardCoded.PHYSICS_JUMP_IMPULSE);
			physics.body.applyLinearImpulse(v, physics.body.getPosition(), true);
			jam.sfx.playSfx(Sfx.PROTAGONIST_JUMP, true);
		}

		// Find out if the player is moving left or right
		Vector2 velocity = physics.body.getLinearVelocity();
		float velocityAngle = velocity.angle();
		float bodyAngle = physics.body.getAngle() * MathUtils.radiansToDegrees;
		/*
		 * Phaseshifted 90 degrees.
		 */
		float localVelocityAngle = ((velocityAngle - bodyAngle + 90f) + 360f) % 360f;
		/*
		 * Left or right. There is no other!
		 */
		boolean movingLeftInsteadOfRight = localVelocityAngle > 180f;
		/*
		 * XXX: This is not 100% true, but it'll suffice.
		 */
		boolean standingStill = velocity.len2() > HardCoded.ANIMATION_WALKSPEED_TRESHOLD;

		/*
		 * Throw stuff
		 */
		control.output.secondsSinceLastThrow += deltaTime;
		boolean shouldThrow = control.input.thrw
				&& control.output.secondsSinceLastThrow > HardCoded.PARAMS_THROW_COOLDOWN;
		boolean throwLeftInsteadOfRight = false;

		if (shouldThrow) {
			control.output.secondsSinceLastThrow = 0f;
			/*
			 * Throw direction
			 */
			if (control.input.walkLeft)
				throwLeftInsteadOfRight = true;
			else if (control.input.walkRight)
				throwLeftInsteadOfRight = false;
			else {
				throwLeftInsteadOfRight = movingLeftInsteadOfRight;
			}
			/*
			 * TODO: Spawn a fruit
			 */
			spawnFruit(physics.body.getPosition(), physics.body.getAngle(), throwLeftInsteadOfRight);
			jam.sfx.playSfx(Sfx.PROTAGONIST_THROW, true);
		}

		/*
		 * other...
		 */
		// // cap max velocity on x (todo: on x!)
		// v.set(physics.body.getLinearVelocity());
		// v.limit2(MAX_VELOCITY_SQUARED);
		// physics.body.setLinearVelocity(v);

		// Update player anim
		if (!control.hasLanded) {
			anim.changeAnimation(PlayerAnimations.PARACHUTE.key);
		} else if (shouldThrow) {
			// Throw anim
			if (throwLeftInsteadOfRight)
				anim.changeAnimation(PlayerAnimations.THROW_LEFT.key);
			else
				anim.changeAnimation(PlayerAnimations.THROW_RIGHT.key);
		} else {
			// "normal" anim
			if (grounded) {
				if (standingStill) {
					if (movingLeftInsteadOfRight)
						anim.changeAnimation(PlayerAnimations.WALK_LEFT.key);
					else
						anim.changeAnimation(PlayerAnimations.WALK_RIGHT.key);
				} else
					anim.changeAnimation(PlayerAnimations.STAND.key);
			} else {
				if (movingLeftInsteadOfRight)
					anim.changeAnimation(PlayerAnimations.JUMP_LEFT.key);
				else
					anim.changeAnimation(PlayerAnimations.JUMP_RIGHT.key);
			}
		}

		/*
		 * Check game end
		 */
		if (contacts.playerSensorToLaddersCount > 0) {
			whenWon.run();
		}
		if (control.hasLanded && !gravityTarget.hasGravitySourceAtTheMoment) {
			whenDriftingToSpace.run();
		}

	}

	private void spawnFruit(Vector2 position, float angleUp, boolean throwLeftInsteadOfRight) {
		/*
		 * XXX: This method allocates a lot of stuff. Check what is really
		 * necessary and what is not (maybe migrate to the PooledEngine).
		 */
		Entity fruit = new Entity();

		v.setAngleRad(angleUp);
		v.rotate(90f); // Phase correction
		if (throwLeftInsteadOfRight)
			v.rotate(HardCoded.PARAMS_FRUIT_THROWANGLE);
		else
			v.rotate(-HardCoded.PARAMS_FRUIT_THROWANGLE);
		v.setLength2(HardCoded.PARAMS_FRUIT_THROWSPEEDSQUARED);

		PhysicsComponent p = new PhysicsComponent();
		p.bodyType = BodyType.FRUIT;
		p.creationPoint = position;
		p.bodyTypeParams = new float[] { HardCoded.PARAMS_FRUIT_RADIUS, v.x, v.y,
				2f /* TODO: now a random angular velocity!.. */ };

		fruit.add(p);

		GravityTarget g = new GravityTarget(false);
		fruit.add(g);

		ObjectMap<String, AnimationDetails> fruitAnimation = jam.getFruitAnimation();
		fruit.add(new Animated(fruitAnimation.get(FruitAnimations.STAGE_0.key), fruitAnimation,
				HardCoded.GRAPHICS_LAYERS_FRUIT));

		fruit.add(new FruitData());

		getEngine().addEntity(fruit);

	}

}
