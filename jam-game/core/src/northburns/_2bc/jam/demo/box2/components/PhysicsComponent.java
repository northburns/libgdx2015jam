package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import northburns._2bc.jam.util.Optional;

public class PhysicsComponent implements Component {

	/**
	 * Only used when creating the entity
	 */
	public Vector2 creationPoint;
	/**
	 * In "local coordinates" (un-rotated)
	 */
	public Vector2 posToAnimOrigin = new Vector2();

	/**
	 * Body types list their params in order.
	 *
	 */
	public enum BodyType {
		/**
		 * <ul>
		 * <li>Half-Width (sensor's radius is 10% of this)</li>
		 * <li>Half-Height</li>
		 * </ul>
		 */
		PLAYER,
		/**
		 * <ul>
		 * <li>Radius</li>
		 * <li>Gravity radius (measured from the planet's center)</li>
		 * </ul>
		 * <p>
		 * Gravity force can be found in {@link GravitySource}
		 * </p>
		 */
		PLANET,
		/**
		 * <ul>
		 * <li>Radius</li>
		 * <li>Initial velocity, x</li>
		 * <li>Initial velocity, y</li>
		 * <li>Initial angular velocity</li>
		 * </ul>
		 */
		FRUIT,
		/**
		 * <ul>
		 * <li>Half-Width</li>
		 * <li>Half-Height</li>
		 * </ul>
		 */
		FLY,
	}

	public BodyType bodyType;
	public float[] bodyTypeParams;

	public Body body;
	public Fixture bodyFixture;
	public Optional<Fixture> sensorFixture;

}
