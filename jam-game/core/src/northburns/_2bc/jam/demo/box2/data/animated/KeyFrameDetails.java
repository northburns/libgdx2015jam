package northburns._2bc.jam.demo.box2.data.animated;

public class KeyFrameDetails {
	public String frame;
	public int drawZeroX, drawZeroY;
	public float worldWidth, worldHeight;

	public KeyFrameDetails() {
	}

	public KeyFrameDetails(String frame) {
		this.frame = frame;
	}

	public KeyFrameDetails(String frame, int drawZeroX, int drawZeroY, float worldWidth, float worldHeight) {
		this.frame = frame;
		this.drawZeroX = drawZeroX;
		this.drawZeroY = drawZeroY;
		this.worldWidth = worldWidth;
		this.worldHeight = worldHeight;
	}

}