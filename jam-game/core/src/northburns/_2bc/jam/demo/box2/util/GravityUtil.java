package northburns._2bc.jam.demo.box2.util;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsUserData;

public final class GravityUtil {
	private GravityUtil() {
	}

	/**
	 * @return one of the entities, if it is in the contact (by its body in the
	 *         {@link PhysicsComponent}), and the contac't other body isn't
	 *         another entity in the list. Otherwise, returns null.
	 */
	public static Entity getExactlyOneContactEntity(Contact contact, Iterable<Entity> gravities,
			ComponentMapper<PhysicsComponent> cmP) {
		Entity resultA = null, resultB = null;
		for (Entity e : gravities) {
			PhysicsComponent pc = cmP.get(e);
			if (pc.body == contact.getFixtureA().getBody())
				resultA = e;
			if (pc.body == contact.getFixtureB().getBody())
				resultB = e;
			if (resultA != null && resultB != null)
				break;
		}
		if (resultA != null && resultB != null)
			return null;
		if (resultA != null)
			return resultA;
		if (resultB != null)
			return resultB;
		return null;
	}

	public static boolean isThisBodyInContact(Contact contact, Body body) {
		return contact.getFixtureA().getBody() == body || contact.getFixtureB().getBody() == body;
	}

	public static boolean isThisFixtureInContact(Contact contact, Fixture fixture) {
		return contact.getFixtureA() == fixture || contact.getFixtureB() == fixture;
	}

	public static Fixture getOtherFixture(Contact contact, Fixture thisFixture) {
		if (contact.getFixtureA() == thisFixture)
			return contact.getFixtureB();
		if (contact.getFixtureB() == thisFixture)
			return contact.getFixtureA();
		throw new IllegalArgumentException("Passed fixture was neither in contact.");
	}

	public static Entity getFixturesBodysEntity(Fixture f) {
		Object userData = f.getBody().getUserData();
		if (userData instanceof PhysicsUserData)
			return ((PhysicsUserData) userData).entity;
		return null;
	}

	/**
	 */
	public static Fixture ifIsPlayerControlledSensorFixtureTouching(Contact contact) {
		Entity entityA = getFixturesBodysEntity(contact.getFixtureA());
		Entity entityB = getFixturesBodysEntity(contact.getFixtureB());

		ComponentMapper<PlayerControlled> cmPlayer = ComponentMappers.PLAYER_CONTROLLED;

		if (cmPlayer.has(entityA) && cmPlayer.has(entityB))
			throw new UnsupportedOperationException("Two player controlled entities not supported.");

		if (!cmPlayer.has(entityA) && !cmPlayer.has(entityB))
			// neither player controlled
			return null;

		Entity playerControlled = cmPlayer.has(entityA) ? entityA : entityB;

		PhysicsComponent playerPhysics = ComponentMappers.PHYSICS_COMPONENT.get(playerControlled);

		Fixture playerSensor = playerPhysics.sensorFixture.get();
		if (contact.getFixtureA() != playerSensor && contact.getFixtureB() != playerSensor)
			// neither fixture is player's sensor
			return null;

		Fixture contactFixture = contact.getFixtureA() == playerSensor ? contact.getFixtureB() : contact.getFixtureA();

		return contactFixture;
	}

}
