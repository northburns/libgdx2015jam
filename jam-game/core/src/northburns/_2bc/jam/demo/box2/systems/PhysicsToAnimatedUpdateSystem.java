package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;

public class PhysicsToAnimatedUpdateSystem extends IteratingSystem {

	public PhysicsToAnimatedUpdateSystem(int priority) {
		super(Family.all(PhysicsComponent.class, Animated.class).get(), priority);
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PhysicsComponent phys = ComponentMappers.PHYSICS_COMPONENT.get(entity);
		Animated anim = ComponentMappers.ANIMATED.get(entity);

		anim.pos.set(phys.body.getPosition());
		anim.posToAnimOrigin = phys.posToAnimOrigin;
		anim.rotation = phys.body.getAngle();
	}
}
