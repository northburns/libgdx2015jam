package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class GravityTarget implements Component {

	public GravityTarget(boolean pointToGravitySource) {
		this.pointToGravitySource = pointToGravitySource;
	}

	/**
	 * If true and target of two or more gravities, points to whichever is
	 * stronger.
	 */
	public boolean pointToGravitySource;

	public boolean hasGravitySourceAtTheMoment;
	public Vector2 strongestGravitySourcePoint = new Vector2();
}
