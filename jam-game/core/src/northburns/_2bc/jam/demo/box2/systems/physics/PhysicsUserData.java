package northburns._2bc.jam.demo.box2.systems.physics;

import com.badlogic.ashley.core.Entity;

public class PhysicsUserData {
	public final Entity entity;

	public PhysicsUserData(Entity entity) {
		this.entity = entity;
	}

}
