package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;

import northburns._2bc.jam.demo.box2.components.CameraComponent;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.util.JamMathUtils;

public class CameraUpdateSystem extends EntitySystem {

	private static final Family family = Family.all(PlayerControlled.class, PhysicsComponent.class).get();
	private static final Family FAMILY_CAMERA = Family.all(CameraComponent.class).get();

	/**
	 * TODO: Should deltaTime be taken into account with these?.. Yes, but how.
	 */
	private static final float CAMERA_ANGLE_SPEED = .5f;
	private static final float CAMERA_POS_SPEED = .5f;

	private static final Interpolation CAMERA_ANGLE_INTERPOLATION = Interpolation.exp10In;
	private static final Interpolation CAMERA_POS_INTERPOLATION = Interpolation.exp10In;

	private float lastSetAngle = 0f;
	private CameraComponent cameraComponent;

	private Vector3 v3 = new Vector3();

	public CameraUpdateSystem(int priority) {
		super(priority);
	}

	@Override
	public void update(float deltaTime) {
		ImmutableArray<Entity> cameraEntities = getEngine().getEntitiesFor(FAMILY_CAMERA);
		if (cameraEntities.size() != 1)
			throw new IllegalStateException("Number of Camera entities was not 1.");
		this.cameraComponent = ComponentMappers.CAMERA.get(cameraEntities.first());

		ImmutableArray<Entity> players = getEngine().getEntitiesFor(family);
		if (players.size() != 1)
			throw new IllegalStateException("Player count other than 1 not supported.");
		Entity player = players.first();

		@SuppressWarnings("unused")
		PlayerControlled playerControlled = ComponentMappers.PLAYER_CONTROLLED.get(player);
		PhysicsComponent physicsComponent = ComponentMappers.PHYSICS_COMPONENT.get(player);
		GravityTarget gravityTarget = ComponentMappers.GRAVITY_TARGET.get(player);

		Body body = physicsComponent.body;

		float cameraAngle = -(body.getAngle() * MathUtils.radiansToDegrees);

		lastSetAngle = cameraAngle = JamMathUtils.interpolateAngleDeg(lastSetAngle, cameraAngle, CAMERA_ANGLE_SPEED,
				CAMERA_ANGLE_INTERPOLATION);

		v3.set(body.getPosition(), 0f);
		if (gravityTarget.hasGravitySourceAtTheMoment) {
			v3.add(gravityTarget.strongestGravitySourcePoint.x, gravityTarget.strongestGravitySourcePoint.y, 0)
					.scl(.5f);
		}

		OrthographicCamera camera = cameraComponent.camera;

		/*
		 * These two thingies "reset" the camera. This is necessary 'cause
		 * rotate() acts on the camera's current state.
		 * (http://gamedev.stackexchange.com/a/77728)
		 */
		/*
		 * TODO: Understand the math behind these.
		 */
		camera.up.set(0, 1, 0);
		camera.direction.set(0, 0, -1);

		camera.position.interpolate(v3, CAMERA_POS_SPEED, CAMERA_POS_INTERPOLATION);
		camera.rotate(cameraAngle);
		cameraComponent.lastSetRotation = cameraAngle;
		camera.zoom = .12f; // TODO: Zoom?
		camera.zoom = .077f;

		camera.update();
	}

}
