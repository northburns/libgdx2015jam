package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponentRope;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;

public class GravitySystem extends EntitySystem {

	private Vector2 v = new Vector2();
	private Contacts contacts;

	public GravitySystem(Contacts contacts, int priority) {
		super(priority);
		this.contacts = contacts;
	}

	@Override
	public void update(float deltaTime) {

		for (Entity targetEntity : contacts.gravityTargetsSources.keys()) {
			GravityTarget target = ComponentMappers.GRAVITY_TARGET.get(targetEntity);
			PhysicsComponent targetP = ComponentMappers.PHYSICS_COMPONENT.get(targetEntity);
			PhysicsComponentRope targetR = ComponentMappers.PHYSICS_COMPONENT_ROPE.get(targetEntity);

			Array<Entity> sourceEntities = contacts.gravityTargetsSources.get(targetEntity);
			target.hasGravitySourceAtTheMoment = sourceEntities.size > 0;

			for (Entity sourceEntity : sourceEntities) {
				float force = ComponentMappers.GRAVITY_SOURCE.get(sourceEntity).force;
				Vector2 sourcePoint = ComponentMappers.PHYSICS_COMPONENT.get(sourceEntity).body.getPosition();
				if (targetP != null)
					applyGravity(targetP.body, sourcePoint, force);
				if (targetR != null)
					for (Body b : targetR.physics.bodies)
						applyGravity(b, sourcePoint, force);
			}

		}

	}

	private void applyGravity(Body target, Vector2 gravityPoint, float force) {
		v.set(target.getPosition());
		v.sub(gravityPoint);
		v.clamp(1f, 1f).scl(-force);
		target.applyForceToCenter(v, true);
	}

}
