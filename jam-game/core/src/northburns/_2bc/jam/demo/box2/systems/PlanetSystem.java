package northburns._2bc.jam.demo.box2.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;

import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.PlanetAnimations;
import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.PlanetComponent;
import northburns._2bc.jam.demo.box2.components.util.ComponentMappers;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem.Contacts;
import northburns._2bc.jam.resourcehelp.JamResources;

public class PlanetSystem extends IteratingSystem {

	@SuppressWarnings("unused")
	private Contacts contacts;

	@SuppressWarnings("unused")
	private JamResources jam;

	/**
	 * XXX: I dislike couplign these two systems like this.
	 */
	@SuppressWarnings("unused")
	private PhysicsSystem physicsSystem;

	public PlanetSystem(PhysicsSystem physicsSystem, Contacts contacts, JamResources jam, int priority) {
		super(Family.all(PlanetComponent.class).get(), priority);
		this.physicsSystem = physicsSystem;
		this.contacts = contacts;
		this.jam = jam;
	}

	/**
	 * A temp vector used for calcs. No need to pollute the heap.
	 */
	@SuppressWarnings("unused")
	private Vector2 v = new Vector2();

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PlanetComponent planet = ComponentMappers.PLANET.get(entity);
		Animated face = ComponentMappers.ANIMATED.get(planet.face);
		if (planet.health >= HardCoded.PARAMS_PLANET_HEALTH) {
			face.changeAnimation(PlanetAnimations.FACE_SMILE.key);
		} else if (planet.health >= HardCoded.PARAMS_PLANET_HEALTH_FROWN_TRESHOLD) {
			face.changeAnimation(PlanetAnimations.FACE_NEUTRAL.key);
		} else {
			face.changeAnimation(PlanetAnimations.FACE_FROWN.key);
		}
	}

}
