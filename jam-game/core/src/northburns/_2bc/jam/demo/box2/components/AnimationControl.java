package northburns._2bc.jam.demo.box2.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.IntMap;

public class AnimationControl implements Component {
	public IntMap<ShaderProgram> layerShaders = new IntMap<>();
	public IntMap<ShaderApplier> layerShaderAppliers = new IntMap<>();
	public boolean renderDebug;

	public static abstract class ShaderApplier {
		public abstract void apply(float deltaTime, ShaderProgram shader);
	}
}
