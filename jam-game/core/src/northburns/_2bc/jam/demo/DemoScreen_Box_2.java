package northburns._2bc.jam.demo;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FillViewport;
import northburns._2bc.jam.constants.FlyAnimations;
import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.PlanetAnimations;
import northburns._2bc.jam.constants.PlayerAnimations;
import northburns._2bc.jam.constants.TextureAtlasConstants;
import northburns._2bc.jam.demo.GameEnder.LoseState;
import northburns._2bc.jam.demo.box2.components.Animated;
import northburns._2bc.jam.demo.box2.components.AnimationControl;
import northburns._2bc.jam.demo.box2.components.AnimationControl.ShaderApplier;
import northburns._2bc.jam.demo.box2.components.CameraComponent;
import northburns._2bc.jam.demo.box2.components.FlyData;
import northburns._2bc.jam.demo.box2.components.GravitySource;
import northburns._2bc.jam.demo.box2.components.GravityTarget;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent;
import northburns._2bc.jam.demo.box2.components.PhysicsComponent.BodyType;
import northburns._2bc.jam.demo.box2.components.PlanetComponent;
import northburns._2bc.jam.demo.box2.components.PlayerControlled;
import northburns._2bc.jam.demo.box2.components.PlayerControlled.InputCode;
import northburns._2bc.jam.demo.box2.data.animated.AnimationDetails;
import northburns._2bc.jam.demo.box2.systems.AnimationRenderSystem;
import northburns._2bc.jam.demo.box2.systems.CameraUpdateSystem;
import northburns._2bc.jam.demo.box2.systems.FlySystem;
import northburns._2bc.jam.demo.box2.systems.FruitSystem;
import northburns._2bc.jam.demo.box2.systems.GravitySystem;
import northburns._2bc.jam.demo.box2.systems.InputSystem;
import northburns._2bc.jam.demo.box2.systems.InputSystem.InputDebugStuff;
import northburns._2bc.jam.demo.box2.systems.PhysicsToAnimatedUpdateSystem;
import northburns._2bc.jam.demo.box2.systems.PlanetSystem;
import northburns._2bc.jam.demo.box2.systems.PlayerControlSystem;
import northburns._2bc.jam.demo.box2.systems.RopeRenderSystem;
import northburns._2bc.jam.demo.box2.systems.physics.PhysicsSystem;
import northburns._2bc.jam.input.InputChartImplementation;
import northburns._2bc.jam.resourcehelp.JamResources;
import northburns._2bc.jam.resourcehelp.JamResources.Song;
import northburns._2bc.jam.resourcehelp.ShaderSupplier.ShaderKey;

public class DemoScreen_Box_2 extends AbstractDemoScreen {

	@SuppressWarnings("unused")
	private FPSLogger fps;

	private JamResources res;

	private GameEnder gameEnder;

	public DemoScreen_Box_2(GameEnder gameEnder, JamResources res, SpriteBatch batch) {
		super(new FillViewport(HardCoded.VIEWPORT_WORLD_WIDTH, HardCoded.VIEWPORT_WORLD_HEIGHT), batch, res.assets);
		this.gameEnder = gameEnder;
		this.res = res;

	}

	private InputChartImplementation<InputCode> input;

	private Engine engine;

	private PhysicsSystem physicsSystem;

	private AnimationRenderSystem animSystem;
	private long gameStartMillis;
	private boolean wonByDebugKey;

	private PlanetComponent planet;

	private boolean gameOver;
	private DelayAction gameOverByDriftRunnableAction = new DelayAction(HardCoded.WINLOSE_DRIFTING_GAMEOVERDELAY);

	@Override
	public void show() {
		dispose();
		fps = new FPSLogger();
		res.playSong(Song.LEVEL);
		res.backgroundRenderer.startMoving();

		wonByDebugKey = false;

		input = new InputChartImplementation<>();
		input.setInputKey(InputCode.JUMP, Keys.W);
		input.setInputKey(InputCode.THROW, Keys.SPACE);
		input.setInputKey(InputCode.WALK_LEFT, Keys.A);
		input.setInputKey(InputCode.WALK_RIGHT, Keys.D);

		input.setInputKey(InputCode.DEBUG_TOGGLE_BOX2D, Keys.G);
		input.setInputKey(InputCode.DEBUG_INSTANTWIN, Keys.H);

		final Runnable whenWon = new Runnable() {

			@Override
			public void run() {
				if (!gameOver) {
					gameOver = true;
					gameEnder.win(TimeUtils.millis() - gameStartMillis,
							(float) planet.health / HardCoded.PARAMS_PLANET_HEALTH, wonByDebugKey);
				}
			}
		};
		Runnable whenDriftedToSpace = new Runnable() {

			@Override
			public void run() {
				if (!gameOver) {
					gameOver = true;
					gameOverByDriftRunnableAction.restart();
					RunnableAction action = new RunnableAction();
					action.setRunnable(new Runnable() {

						@Override
						public void run() {
							gameEnder.lose(LoseState.DRIFT_TO_SPACE);
						}
					});
					gameOverByDriftRunnableAction.setAction(action);
				}
			}
		};
		Runnable whenPlanetDies = new Runnable() {

			@Override
			public void run() {
				if (!gameOver) {
					gameOver = true;
					gameOverByDriftRunnableAction = new DelayAction(HardCoded.WINLOSE_PLANETDIES_GAMEOVERDELAY);
					RunnableAction action = new RunnableAction();
					action.setRunnable(new Runnable() {

						@Override
						public void run() {
							gameEnder.lose(LoseState.PLANET_HP_ZERO);
						}
					});
					gameOverByDriftRunnableAction.setAction(action);
				}
			}
		};
		InputDebugStuff inputDebugStuff = new InputDebugStuff() {

			@Override
			public void winNow() {
				wonByDebugKey = true;
				whenWon.run();
			}
		};

		engine = new Engine();
		int priority = 0;
		physicsSystem = new PhysicsSystem(Integer.MAX_VALUE);
		engine.addSystem(new InputSystem(input, inputDebugStuff, priority++));
		engine.addSystem(new GravitySystem(physicsSystem.contacts, priority++));
		engine.addSystem(new PlayerControlSystem(res, physicsSystem.contacts, whenWon, whenDriftedToSpace, priority++));
		engine.addSystem(new FlySystem(physicsSystem, physicsSystem.contacts, res, priority++));
		engine.addSystem(new FruitSystem(physicsSystem, physicsSystem.contacts, res, whenPlanetDies, priority++));
		engine.addSystem(new PlanetSystem(physicsSystem, physicsSystem.contacts, res, priority++));
		engine.addSystem(new PhysicsToAnimatedUpdateSystem(priority++));
		engine.addSystem(new CameraUpdateSystem(priority++));
		animSystem = new AnimationRenderSystem(assets, res, viewport, physicsSystem.world, priority++);
		engine.addSystem(animSystem);
		engine.addSystem(
				new RopeRenderSystem(physicsSystem, physicsSystem.contacts, res, viewport, animSystem, priority++));
		engine.addSystem(physicsSystem);
		engine.addEntityListener(PhysicsSystem.family, physicsSystem);

		/*
		 * XXX: Sure, create and initialize all of the game entities in the
		 * show()-method.. move this stuff into a more appropriate place.
		 */
		/*
		 * XXX: Also, the stuff with the *Animations has a lot of unnecessary
		 * repetition going on!
		 */

		Entity cameraEntity = new Entity();
		CameraComponent cameraComponent = new CameraComponent(camera);
		cameraEntity.add(cameraComponent);
		engine.addEntity(cameraEntity);

		Entity playerCharacter = new Entity();
		playerCharacter.add(new PlayerControlled());
		{
			PhysicsComponent physics_body = new PhysicsComponent();
			physics_body.bodyType = PhysicsComponent.BodyType.PLAYER;
			/*
			 * XXX: Move player's physics&anim params somewhere?
			 */
			physics_body.bodyTypeParams = new float[] { 1f, 1.5f };
			physics_body.creationPoint = new Vector2(10, 30);
			physics_body.posToAnimOrigin.set(0f, 1.5f);
			playerCharacter.add(physics_body);
		}
		playerCharacter.add(new GravityTarget(true));
		TextureAtlas atlasPlayer = assets.get(TextureAtlasConstants.PROTAGONIST.assetDescriptor);
		{
			ObjectMap<String, AnimationDetails> playerAnims = new ObjectMap<>();
			for (PlayerAnimations pa : PlayerAnimations.values()) {
				AnimationDetails ad = AnimationDetails
						.fromJson(Gdx.files.internal("anim/protagonist/" + pa.key + ".json").readString());
				ad.initAnimation(atlasPlayer);
				playerAnims.put(pa.key, ad);
			}
			playerCharacter.add(new Animated(playerAnims.get(PlayerAnimations.PARACHUTE.key), playerAnims, 9));
		}
		engine.addEntity(playerCharacter);

		Entity planetEntity = new Entity();
		{
			PhysicsComponent physicsPlanet = new PhysicsComponent();
			physicsPlanet.bodyType = BodyType.PLANET;
			physicsPlanet.bodyTypeParams = new float[] { HardCoded.PARAMS_PLANET_RADIUS,
					HardCoded.PARAMS_GRAVITYFIELD_RADIUS };
			physicsPlanet.creationPoint = new Vector2(0f, 0f);
			planetEntity.add(physicsPlanet);
		}
		planetEntity.add(new GravitySource(HardCoded.PARAMS_GRAVITYFIELD_GRAVITYFORCE));
		TextureAtlas atlasPlanet = assets.get(TextureAtlasConstants.PLANET.assetDescriptor);
		ObjectMap<String, AnimationDetails> planetAnims = new ObjectMap<>();
		{
			for (PlanetAnimations pa : PlanetAnimations.values()) {
				AnimationDetails ad = AnimationDetails
						.fromJson(Gdx.files.internal("anim/planet/" + pa.key + ".json").readString());
				ad.initAnimation(atlasPlanet);
				planetAnims.put(pa.key, ad);
			}
		}

		planetEntity.add(new Animated(planetAnims.get(PlanetAnimations.PLANET.key), planetAnims, 4));
		engine.addEntity(planetEntity);

		Entity planetFaceEntity = new Entity();
		{
			Animated animated = new Animated(planetAnims.get(PlanetAnimations.FACE_SMILE.key), planetAnims, 4);
			animated.rotateFromCamera = true;
			planetFaceEntity.add(animated);
		}
		engine.addEntity(planetFaceEntity);
		Entity planetAirEntity = new Entity();
		{
			final Animated airAnimated = new Animated(planetAnims.get(PlanetAnimations.AIR.key), planetAnims, 1);
			airAnimated.rotateFromCamera = false;
			planetAirEntity.add(airAnimated);
		}
		engine.addEntity(planetAirEntity);
		Entity planetGravityEntity = new Entity();
		{
			Animated animated = new Animated(planetAnims.get(PlanetAnimations.GRAVITY.key), planetAnims, 2);
			animated.rotateFromCamera = false;
			planetGravityEntity.add(animated);
		}
		engine.addEntity(planetGravityEntity);

		planet = new PlanetComponent(HardCoded.PARAMS_PLANET_HEALTH, planetFaceEntity, planetGravityEntity,
				planetAirEntity);
		planetEntity.add(planet);

		Entity flyEntity = new Entity();
		TextureAtlas atlasFly = assets.get(TextureAtlasConstants.FLY.assetDescriptor);
		{
			ObjectMap<String, AnimationDetails> flyAnims = new ObjectMap<>();
			for (FlyAnimations pa : FlyAnimations.values()) {
				AnimationDetails ad = AnimationDetails
						.fromJson(Gdx.files.internal("anim/fly/" + pa.key + ".json").readString());
				ad.initAnimation(atlasFly);
				flyAnims.put(pa.key, ad);
			}
			flyEntity.add(new Animated(flyAnims.get(FlyAnimations.FLY_LEFT_SIZE10.key), flyAnims, 100));
		}
		{
			FlyData flyData = new FlyData();
			flyData.pivotPoint = planetEntity;
			flyData.altitudeChangeAmplitude = HardCoded.PARAMS_FLY_ALTITUTE_AMPL;
			flyData.altitudeChangeFrequency = HardCoded.PARAMS_FLY_ALTITUTE_FREQ;
			flyData.angleDeltaCoefficient = HardCoded.PARAMS_FLY_ANGULARVEL;
			flyData.distanceZero = HardCoded.PARAMS_FLY_RADIUS;
			flyEntity.add(flyData);

			flyEntity.add(FlySystem.createFlyPhysics(flyData.size.scale));
		}
		engine.addEntity(flyEntity);

		/*
		 * TODO: Reemove this
		 */
		// Spawn ladder
		// Entity ropeEntity = new Entity();
		// {
		// PhysicsComponentRope r = new PhysicsComponentRope();
		// r.flyToFollow = flyEntity;
		// ropeEntity.add(r);
		// ropeEntity.add(new GravityTarget(false));
		// }
		// engine.addEntity(ropeEntity);

		Entity animationControlEntity = new Entity();
		{
			// 1 = air
			// 2 = gravity
			// 4 = planet
			AnimationControl ac = new AnimationControl();
			ac.layerShaders.put(1, res.shaders.get(ShaderKey.GRAYCOLOR));
			ac.layerShaders.put(2, res.shaders.get(ShaderKey.GRAYCOLOR));
			ac.layerShaders.put(4, res.shaders.get(ShaderKey.GRAYCOLOR));

			final Color COLOR_GREEN = new Color(.45f, .63f, .37f, 1f);

			ac.layerShaderAppliers.put(1, new ShaderApplier() {

				@Override
				public void apply(float deltaTime, ShaderProgram shader) {
					float h = 1f - (float) planet.health / (float) HardCoded.PARAMS_PLANET_HEALTH;

					shader.setUniformf("color1", 1f, 1f, 1f, 0f);
					shader.setUniformf("color2", COLOR_GREEN.r, COLOR_GREEN.g, COLOR_GREEN.b, h);
				}
			});
			ac.layerShaderAppliers.put(2, new ShaderApplier() {

				/**
				 * Working color to preserve the others.
				 */
				private final Color C = Color.BLACK.cpy();

				@Override
				public void apply(float deltaTime, ShaderProgram shader) {
					float h = 1f - (float) planet.health / (float) HardCoded.PARAMS_PLANET_HEALTH;
					C.set(Color.WHITE).lerp(COLOR_GREEN, h);

					shader.setUniformf("color1", 0f, 0f, 0f, 0f);
					shader.setUniformf("color2", C.r, C.g, C.b, 1f);
				}
			});

			ac.layerShaderAppliers.put(4, new ShaderApplier() {

				@Override
				public void apply(float deltaTime, ShaderProgram shader) {
					shader.setUniformf("color1", .17f, .37f, .24f, 0f);
					shader.setUniformf("color2", 1f, 1f, .8f, 1f);
				}
			});

			animationControlEntity.add(ac);
		}
		engine.addEntity(animationControlEntity);

		Gdx.input.setInputProcessor(input);

		gameStartMillis = TimeUtils.millis();
		gameOver = false;
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		res.backgroundRenderer.draw(batch, delta);

		engine.update(delta);

		// fps.log();
		gameOverByDriftRunnableAction.act(delta);
	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		if (animSystem != null)
			animSystem.dispose();
		if (physicsSystem != null)
			physicsSystem.dispose();
	}
}
