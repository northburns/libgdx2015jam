package northburns._2bc.jam.demo;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import northburns._2bc.jam.constants.HardCoded;
import northburns._2bc.jam.constants.TextureConstants;

public class BackgroundRenderer {

	private Texture spaceBg;
	private float bgAspectRatio;

	private final Vector3 v3 = new Vector3();
	private ScreenViewport viewport;

	public BackgroundRenderer(AssetManager assets) {
		assets.load(TextureConstants.SPACEBG_1.assetDescriptor);
		assets.finishLoading();
		spaceBg = assets.get(TextureConstants.SPACEBG_1.assetDescriptor);
		spaceBg.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		bgAspectRatio = (float) spaceBg.getWidth() / (float) spaceBg.getHeight();

		viewport = new ScreenViewport();

	}

	boolean move = false;
	float desiredSpot;
	private float currentSrcX;

	public void stopAndSetRandom() {
		move = false;
		desiredSpot = (desiredSpot + (MathUtils.random(.25f, .75f) * spaceBg.getWidth())) % spaceBg.getWidth();
	}

	public void startMoving() {
		move = true;
	}

	public void draw(Batch batch, float delta) {
		if (move) {
			currentSrcX += HardCoded.BACKGROUND_SCROLL_COEFFICIENT * delta;
		} else {
			currentSrcX = Interpolation.exp10Out.apply(currentSrcX, desiredSpot, delta * .5f);
		}

		render((int) currentSrcX, batch);
	}

	private void render(int srcX, Batch batch) {
		Camera cam = viewport.getCamera();
		cam.update();
		batch.setProjectionMatrix(cam.combined);
		batch.setTransformMatrix(cam.view);
		batch.setShader(null);

		viewport.apply();

		batch.begin();

		v3.set(viewport.getScreenWidth(), viewport.getScreenHeight(), 0f);

		float x = cam.position.x - v3.x * .5f;
		float y = cam.position.y - v3.y * .5f;

		int srcY = 0;
		int srcWidth = spaceBg.getWidth();
		int srcHeight = spaceBg.getHeight();
		float height = v3.y;
		float width = bgAspectRatio * height;
		batch.draw(spaceBg, x, y, width, height, srcX, srcY, srcWidth, srcHeight, false, false);

		batch.end();

	}

	public void resize(int width, int height) {
		viewport.update(width, height);
	}

}
