package northburns._2bc.jam;

public interface Screens {
	public enum ScreenName {
		GAME, TITLE, WIN, LOSE, INTRO
	}

	void changeScreen(ScreenName screen);
}