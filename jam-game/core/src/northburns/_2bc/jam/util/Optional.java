package northburns._2bc.jam.util;

import java.util.NoSuchElementException;

/**
 * Something like you'd see in Java 8 or in Guava. A slimmed down version,
 * though. Really missing in features.
 *
 */
public class Optional<T> {

	public static <T> Optional<T> fromNullable(T t) {
		if (t == null)
			return empty();
		else
			return new Optional<T>(t);
	}

	public static <T> Optional<T> empty() {
		@SuppressWarnings("unchecked")
		Optional<T> empty = (Optional<T>) EMPTY;
		return empty;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final Optional EMPTY = new Optional(null);
	private T t;

	private Optional(T t) {
		this.t = t;
	}

	public boolean isPresent() {
		return t != null;
	}

	public T get() {
		if (t == null)
			throw new NoSuchElementException();
		return t;
	}

}
