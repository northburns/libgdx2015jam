package northburns._2bc.jam.util;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;

public final class JamMathUtils {
	private JamMathUtils() {
	}

	/**
	 * Like {@link MathUtils#lerpAngleDeg(float, float, float)} but with a
	 * custom {@link Interpolation}.
	 */
	public static float interpolateAngleDeg(float fromDegrees, float toDegrees, float progress,
			Interpolation interpolation) {
		float delta = ((toDegrees - fromDegrees + 360 + 180) % 360) - 180;
		float m = interpolation.apply(0f, delta, progress);
		return (fromDegrees + m + 360) % 360;
	}

}
