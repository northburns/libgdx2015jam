package northburns._2bc.jam.resourcehelp.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ArrayMap;

import northburns._2bc.jam.resourcehelp.ShaderSupplier;

public class ShaderSupplierImpl implements ShaderSupplier {
	private final ArrayMap<ShaderKey, ShaderProgram> shaders;

	public ShaderSupplierImpl() {
		shaders = new ArrayMap<>(ShaderKey.values().length);
		loadAndCompileAllShaders();
	}

	private void loadAndCompileAllShaders() {

		shaders.put(ShaderKey.DISTANCEFIELD, new ShaderProgram(Gdx.files.internal("shaders/distancefield.vert"),
				Gdx.files.internal("shaders/distancefield.frag")));
		shaders.put(ShaderKey.GRAYCOLOR, new ShaderProgram(Gdx.files.internal("shaders/graycolor.vert"),
				Gdx.files.internal("shaders/graycolor.frag")));

		for (ShaderProgram sp : shaders.values())
			if (!sp.isCompiled()) {
				String message = "compilation failed:\n" + sp.getLog();
				Gdx.app.error("ShaderProvider", message);
				throw new RuntimeException(message);
			}
	}

	@Override
	public ShaderProgram get(ShaderKey shader) {
		if (!shaders.containsKey(shader))
			throw new IllegalStateException("No shader: " + shader);
		return shaders.get(shader);
	}

	@Override
	public void dispose() {
		for (ShaderProgram sp : shaders.values())
			sp.dispose();
	}

}
