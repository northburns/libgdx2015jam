package northburns._2bc.jam.resourcehelp;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Disposable;

import northburns._2bc.jam.resourcehelp.ShaderSupplier.ShaderKey;

public class JamFont implements Disposable {

	private Texture texture;
	private ShaderProgram fontShader;
	private BitmapFont font;
	private ShaderProgram savedShader;

	public JamFont(ShaderSupplier shaders) {
		/*
		 * https://github.com/libgdx/libgdx/wiki/Distance-field-fonts#loading-
		 * the-font
		 */
		texture = new Texture(Gdx.files.internal("fonts/kenpixel_mini_df.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		font = new BitmapFont(Gdx.files.internal("fonts/kenpixel_mini_df.fnt"), new TextureRegion(texture), false);

		fontShader = shaders.get(ShaderKey.DISTANCEFIELD);

	}

	@Override
	public void dispose() {
		texture.dispose();
		font.dispose();
	}

	public void begin(Batch batch) {
		savedShader = batch.getShader();
		batch.setShader(fontShader);
	}

	public void end(Batch batch) {
		batch.setShader(savedShader);
	}

	public void setColor(Color c) {
		font.setColor(c);
	}

	public void draw(Batch batch, String string, float x, float y) {
		draw(batch, string, x, y, 1f);
	}

	public void draw(Batch batch, String string, float x, float y, float scale) {
		draw(batch, string, x, y, scale, scale);
	}

	public void draw(Batch batch, String string, float x, float y, float scaleX, float scaleY) {
		font.getData().setScale(scaleX, scaleY);
		font.draw(batch, string, x, y);
	}

}
