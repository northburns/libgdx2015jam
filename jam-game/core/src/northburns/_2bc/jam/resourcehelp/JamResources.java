package northburns._2bc.jam.resourcehelp;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

import northburns._2bc.jam.Screens;
import northburns._2bc.jam.constants.FruitAnimations;
import northburns._2bc.jam.constants.TextureAtlasConstants;
import northburns._2bc.jam.demo.BackgroundRenderer;
import northburns._2bc.jam.demo.SoundEffectPlayer;
import northburns._2bc.jam.demo.box2.data.animated.AnimationDetails;

public class JamResources implements Disposable {

	private static final boolean MUSIC_ENABLED = true;

	public final AssetManager assets;
	public final JamFont font;
	public final ShaderSupplier shaders;
	public final Screens screens;

	public final SoundEffectPlayer sfx;
	public final BackgroundRenderer backgroundRenderer;

	public final Skin skin;

	private ObjectMap<String, AnimationDetails> fruitAnims;

	public JamResources(AssetManager assets, JamFont font, ShaderSupplier shaders, Screens screens) {
		this.assets = assets;
		this.font = font;
		this.shaders = shaders;
		this.screens = screens;

		skin = new Skin(Gdx.files.internal("ui/skin/robinson/robinson-skin.json"));
		// skin = new Skin(Gdx.files.internal("ui/skin/default/uiskin.json"));
		// skin.add("font-default", new BitmapFont(), BitmapFont.class);
		// skin.add("default-font", new BitmapFont(), BitmapFont.class);
		// skin.remove("default-font", BitmapFont.class);
		// skin.remove("font-default", BitmapFont.class);
		// ObjectMap<String,BitmapFont> all = skin.getAll(BitmapFont.class);
		// for(String s : all.keys())
		// System.out.println(s);

		sfx = new SoundEffectPlayer();

		backgroundRenderer = new BackgroundRenderer(assets);
	}

	/**
	 * See {@link FruitAnimations} for keys;
	 */
	public ObjectMap<String, AnimationDetails> getFruitAnimation() {
		/*
		 * TODO: A random fruit
		 */
		if (fruitAnims == null) {
			TextureAtlas atlasFruit = assets.get(TextureAtlasConstants.FRUIT_A.assetDescriptor);
			fruitAnims = new ObjectMap<>();
			for (FruitAnimations pa : FruitAnimations.values()) {
				AnimationDetails ad = AnimationDetails
						.fromJson(Gdx.files.internal("anim/fruit-a/" + pa.key + ".json").readString());
				ad.initAnimation(atlasFruit);
				fruitAnims.put(pa.key, ad);
			}
		}
		return fruitAnims;
	}

	public enum Song {
		THEME("music/theme.mp3"), LEVEL("music/scenario-music.mp3"),;
		private String key;

		private Song(String key) {
			this.key = key;
		}
	}

	private Song musicCurrent;
	private Music music;

	public void playSong(Song song) {
		if (!MUSIC_ENABLED)
			return;
		if (song.equals(musicCurrent))
			return;
		if (music != null) {
			music.stop();
			music.dispose();
			music = null;
		}
		if (music == null) {
			music = Gdx.audio.newMusic(Gdx.files.internal(song.key));
			music.setLooping(true);
			music.setVolume(.2f);
		}
		music.play();
		musicCurrent = song;
	}

	@Override
	public void dispose() {
		if (music != null)
			music.dispose();
		sfx.dispose();
	}

	public void resize(int width, int height) {
		backgroundRenderer.resize(width, height);
	}

	public void stopSong() {
		if (music != null)
			music.stop();
	}
}
