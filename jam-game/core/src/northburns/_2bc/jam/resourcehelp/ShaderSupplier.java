package northburns._2bc.jam.resourcehelp;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Disposable;

public interface ShaderSupplier extends Disposable {

	public enum ShaderKey {
		DISTANCEFIELD, GRAYCOLOR
	}

	ShaderProgram get(ShaderKey shader);

}
