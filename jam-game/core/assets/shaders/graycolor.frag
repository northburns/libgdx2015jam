#ifdef GL_ES
precision mediump float;
precision mediump int;
#else
#define highp;
#endif


uniform sampler2D u_texture;
uniform vec4 color1;
uniform vec4 color2;

varying vec4 v_color;
varying vec2 v_texCoord;

const vec3 grayScaleMultiplier = vec3(0.299, 0.587, 0.114);

void main() {
	vec4 texColor = texture2D(u_texture, v_texCoord);
	vec3 gray = vec3(dot(texColor.rgb, grayScaleMultiplier));
	//vec3 color1v = vec3(color1[0],color1[1],color1[2]);
	//vec3 color2v = vec3(color2[0],color2[1],color2[2]);
	//color1v = vec3(1,0,0);
	//color2v = vec3(0,1,1);
	vec3 color1v = vec3(color1);
	vec3 color2v = vec3(color2);
    gl_FragColor = vec4(mix(color1v, color2v, gray), mix(color1[3],color2[3],texColor.a));
    // gl_FragColor = vec4(mix(color1, color2, gray));
}