This is a simple skin I made for a toy project of mine (Friday, since shelfed).

The assets come largely from Kenney' assets:

http://kenney.nl/assets

The font has been changed, but the original file is retained.