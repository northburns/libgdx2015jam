# When Space Gives You Lemons

This is an entry in the LibGDX game jam of 2015/2016 (http://itch.io/jam/libgdxjam)

Dev log entry: http://itch.io/jam/libgdxjam/topic/12419/northburns-2bc-devlog

# Running the game

Run the following command in the `jam-game` dir:

_Windows_ 

```
gradlew contentPipeline run
```

_Mac/Linux_ 

```
./gradlew contentPipeline run
```

The `contentPipeline` is only needed if it has not yet been run once, or when certain graphical assets change.